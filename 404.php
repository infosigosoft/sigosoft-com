<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="About us| Sigosoft"/>
<meta property="og:description" content="Founded in 2014, Sigosoft takes pride in representing our strong company culture. We have successfully developed and deployed hundreds of Mobile Apps all over the world."/>
<meta property="og:url" content="https://www.sigosoft.com/about.php"/>
<meta property="og:site_name" content="Sigosoft"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="Founded in 2014, Sigosoft takes pride in representing our strong company culture. We have successfully developed and deployed hundreds of Mobile Apps all over the world."/>
<meta name="twitter:title" content="Aboutus| Sigosoft" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>404 | Sigosoft</title>
<meta content="Founded in 2014, Sigosoft takes pride in representing our strong company culture. We have successfully developed and deployed hundreds of Mobile Apps all over the world." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	    <!-- favicon -->
    <link rel="shortcut icon" href="https://www.sigosoft.com/assets/img/favicon-sigosoft.webp" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/fonts/flaticon.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/animate.min.css">
    <!-- Owl Carousel -->
    <!-- magnific popup -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/magnific-popup.min.css">
    <!-- aos scoll animation css -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/aos.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/style.min.css">
    <!-- responsive -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/responsive.min.css">

    <link rel="stylesheet" type="text/css" href="https://www.sigosoft.com/assets/css/custom.css">
        <!-- inner pages responsive css -->

    </head>
    <body>
        <style>
            .mtb120{
                margin-top:120px;
                margin-bottom:120px;
            }
        </style>


<!-- header begin -->
        <div class="header-2">
            <div class="topbar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                            <p class="welcome-text">We provide worldwide services 24/7</p>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="support-bars">
                                <ul>
                                    <li>
                                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                                        info@sigosoft.com
                                    </li>
                                    <li>
                                        <span class="icon"><i class="fas fa-phone"></i></span>
                                       +91 9846237384
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottombar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-7 d-xl-block d-lg-block d-flex align-items-center">
                                    <div class="logo">
                                        <a href="https://www.sigosoft.com">
                                            <img src="https://www.sigosoft.com/assets/img/logo-sigosoft.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-5 d-xl-none d-lg-none d-block">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mainmenu">
                                <nav class="navbar navbar-expand-lg">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav ml-auto">
                                            <!--<li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Home
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                <a class="dropdown-item" href="https://www.sigosoft.com/partner-with-us">Partner with Us</a>
                                                
                                                </div>
                                            </li>-->
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com">Home</a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        About
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/about">Our Profile</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/team">Our Team</a>                                                    
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/technologies">Our Technologies</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/partner-with-us">Partner with Us</a>
                                                
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Services
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/mobile-app-development-company-in-calicut-india">Mobile App Consultation</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.com/android-app-development">Native Android App Development</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.com/ios-app-development">Native iOS App Development</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.com/flutter-app-development">Flutter App Development</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.com/react-native-development">React Native Development</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.com/mobile-app-ui-designing">Mobile App UI Designing</a>
                                                     <a class="dropdown-item" href="https://www.sigosoft.com/mobile-app-testing">Mobile App Testing</a>
                                                    <!--<a class="dropdown-item" href="https://www.sigosoft.com/ecommerce-website-development"></a>-->
                                                    <!--<a class="dropdown-item" href="https://www.sigosoft.com/magento-web-development-company"></a>-->
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Products
                                                </a>
                                                <div class="dropdown-menu dmw500" aria-labelledby="navbarDropdown4">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
           <a class="dropdown-item" href="https://www.sigosoft.com/ecommerce-mobile-app-development">E-Commerce Apps</a>
           <a class="dropdown-item" href="https://www.sigosoft.com/loyalty-mobile-app-development">Loyalty Apps</a>
         <a class="dropdown-item" href="https://www.sigosoft.com/e-learning-mobile-app-development">E-Learning Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/community-mobile-app-development">Community Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/supply-chain-mobile-app-development">Supply Chain Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/rent-a-car-mobile-app">Rent a Car Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/telemedicine-app-development">TeleMedicine  Apps</a>
     
   </div>
   <div class="col-md-6">
      <a class="dropdown-item" href="https://www.sigosoft.com/food-delivery-app-development">Food Delivery Apps</a>
     <a class="dropdown-item" href="https://www.sigosoft.com/classified-app-development-classified-app-builder">Classifieds Apps</a>
     <a class="dropdown-item" href="https://www.sigosoft.com/air-ticket-booking-app-for-android-iOS">Flight Booking Apps</a>
   <a class="dropdown-item" href="https://www.sigosoft.com/hotel-booking-app-development">Hotel Booking Apps</a>
  <!-- <a class="dropdown-item" href="https://www.sigosoft.com/gym-fitness-mobile-app-development">Gym & Fitness Apps</a>-->
   <a class="dropdown-item" href="https://www.sigosoft.com/sports-app-development-company">Sports Apps</a>
    <a class="dropdown-item" href="https://www.sigosoft.com/car-wash-app-development">Car Wash Apps</a>
   <a class="dropdown-item" href="https://www.sigosoft.com/van-sales-mobile-application">Van sales Apps</a>
   
   </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php /*<li class="nav-item">
                                                <a class="nav-link" href="partner-with-us">Partner with Us</a>
                                            </li> */?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/portfolio">Portfolio</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/blog">Blog</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/careers">Careers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/contact">Contact</a>
                                            </li>
                                        </ul>
                                        <div class="header-buttons">
                                <a href="https://www.sigosoft.com/contact.php" class="quote-button">Get A Quote</a>
                            </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <style>
 body{
     background:#f2f2f2;
 }
 header{
     background:#fff;
 }
 .list-item{
    background:#fff;
    margin-bottom: 15px;
 }
 #sidebar .widget{
     background:#fff;
    margin-bottom: 15px;
 }
 #home_banner_blog{
     margin-bottom: 15px;
 }
 .container {
    width: 100%;
 }
 .blog {
    padding: 0;
}
                    .header-2 .bottombar .header-buttons {
    width: auto !important;
}
.dmw500{
    width:500px;
}
.banner-2 .banner-content {
    padding: 80px 0;
}
.port{
    background: -webkit-linear-gradient(21deg,#d63438 0,#90191c 100%) !important;
    color: #fff !important;
    height: 50px;
    margin-top: 20px !important;
    padding: 14px !important;
    border-radius: 3px;
}

.header-2 .bottombar .mainmenu .navbar .navbar-nav .nav-item .nav-link.port:before{
      background: #0000;
}
.navbar{
    margin-bottom: 0px;
}
@media only screen and (max-width: 768px){
    .port{
    margin-top: auto !important;
    }
}
                </style>


        
        
        <div class="page-title jarallax black-overlay-20" data-jarallax data-speed="0.6" style="background-image: url('img/content/bgs/bg1.webp');">

    </div>
<div class="container mtb120 thankyou">
       <!-- <div class="row mt-10">
        <div class="jumbotron text-xs-center">-->
        <center>
 <h1 class="display-3">404</h1>
 
  <a href="https://www.sigosoft.com" class="btn btn-info" role="button">Back to Home</a>
  <p class="lead"><strong> </strong> </p>
  
  </center>
</div>
                <!-- footer begin -->
        <div class="footer footer-2">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between">
                    
                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>About Us</h3>
                            <ul>
                                <li>
                                    <a href="https://www.sigosoft.com/about">Our Profile</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/partner-with-us">Partner with Us</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/team">Our Team</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/careers">Careers</a>
                                </li>
                                
                                <li>
                                    <a href="https://www.sigosoft.com/technologies">Technologies</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>Services</h3>
                            <ul>
                                <li>
                                    <a href="https://www.sigosoft.com/mobile-app-development-company-in-calicut-india">Mobile App Development</a>
                                </li>
                               <li>
                                    <a href="https://www.sigosoft.com/ecommerce-website-development">E-Commerce Development</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/magento-development-company">Magento Development</a>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="links-widget">
                            <h3>Products</h3>
                            <div class="row">
                            <ul class="col-md-6">
                                <li>
                                    <a href="https://www.sigosoft.com/ecommerce-mobile-app-development">E-Commerce Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/supply-chain-mobile-app-development">Supply Chain Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/e-learning-mobile-app-development">E-Learning Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/telemedicine-app-development">Online Consultation Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/loyalty-mobile-app-development">Loyalty Apps</a>
                                </li>
                            </ul>
                            <ul class="col-md-6">
                                <li>
                                    <a href="https://www.sigosoft.com/air-ticket-booking-app-for-android-iOS">Flight Booking Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/hotel-booking-app-development">Hotel Booking Apps</a>
                                </li>

                                <li>
                                    <a href="https://www.sigosoft.com/food-delivery-app-development">Food Delivery Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/van-sales-mobile-application">Van Sales Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/classified-app-development-classified-app-builder">Classifieds Apps</a>
                                </li>
                                <!--<li>
                                    <a href="gym-fitness-mobile-app-development-company-in-usa">Gym Fitness Apps</a>
                                </li>-->
                                
                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-8">
                        <div class="about-widget links-widget">
                            <h3>Contact</h3>
                            <ul>
                                <li><a href="tel:+91 9846237384"><i class="fas fa-mobile-alt"></i>+91 9846237384</a></li>
                                <?php /*<li><a href="https://api.whatsapp.com/send?phone=919846237384"><i class="fab fa-whatsapp"></i> +91 9846237384</a></li> */?>
                                <li><a href="mailto:info@sigosoft.com"><i class="far fa-envelope-open"></i> info@sigosoft.com</a></li>
                                <li style="color: #bdbdbd; font-size: 15px; line-height: 25px;"><i class="fas fa-map-marker-alt"></i>
                                    <span>Calicut, Kerala, India.</span><br>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- footer end -->
        
        
        
        
        
        
            <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f022eb4760b2b560e6fc8b8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
        
        
     <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sigosoft",
  "url": "https://www.sigosoft.com",
  "logo": "https://www.sigosoft.com/assets/img/logo-sigosoft.png",
  "sameAs": [
    "https://www.facebook.com/sigosoft",
    "https://twitter.com/sigosoft_social",
    "https://www.instagram.com/sigosoft_/",
    "https://www.youtube.com/channel/UC5y70ytFjNLrYPbSUyUp2Cw",
    "https://www.linkedin.com/company/sigosoft/"
  ]
}
</script>


<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "Sigosoft",
  "image": "https://www.sigosoft.com/assets/img/logo-sigosoft.png",
  "@id": "",
  "url": "https://www.sigosoft.com",
  "telephone": "+91 - 9846237384",
  "priceRange": "$$$$",
  "address": {
    
    "@type": "PostalAddress",
    "streetAddress": "India, Kerala",
    "addressLocality": "Calicut",
    "postalCode": "673019",
    "addressCountry": "IN"
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "00:00",
    "closes": "23:59"
  } 
}
</script>


   
        
         <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176910320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176910320-1');
</script>
        
        

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="badges-wrapper text-center">
                            <ul class="badges">
                                <li><a href="#" target="_blank"><img src="https://www.sigosoft.com/assets/img/badges/1.png" alt="Mobile App Development Company in Kerala"></a></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/2.png" alt="Mobile App Development Company in Calicut"></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/3.png" alt=""></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/4.png" alt=""></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/5.png" alt=""></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/6.png" alt=""></li>
                                <li><a href="#" target="_blank"><img src="https://www.sigosoft.com/assets/img/badges/8.png" alt="Mobile App Development Company in India"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- copyright begin -->
        <div class="copyright">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2014-2020 Sigosoft. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                                
                                <li>
                                    <a class="facebook" href= https://www.facebook.com/sigosoft><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a class="instagram" href=https://www.instagram.com/sigosoft_><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="https://twitter.com/sigosoft_social"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href=https://join.skype.com/invite/Qq1GmNOs9DDV><i class="fab fa-skype"></i></a>
                                </li>
                                 <li>
                                  <a class="facebook" href= https://www.linkedin.com/company/sigosoft/><i class="fab fa-linkedin"></i></a>
                                </li>
                                
                              
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- copyright end -->

        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=919846237384" target="_blank"><img src="https://sigosoft.com/assets/img/whatsapp.png" alt="whatsapp"/></a>
        </div>
        
        
        <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5d71e46eeb1a6b0be60b40dc/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
        <!--End of Tawk.to Script-->
            <!-- jquery -->
        <script src="https://sigosoft.com/assets/js/jquery-3.4.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <!-- owl carousel -->
        <script src="https://sigosoft.com/assets/js/owl.carousel.min.js"></script>
        <!-- magnific popup -->
        <script src="https://sigosoft.com/assets/js/jquery.magnific-popup.min.js"></script>
        <!-- counter up js -->
        <script src="https://sigosoft.com/assets/js/jquery.counterup.min.js"></script>
        <script src="https://sigosoft.com/assets/js/counter-us-activate.min.js"></script>
        <!-- waypoints js-->
        <script src="https://sigosoft.com/assets/js/jquery.waypoints.min.js"></script>
        <!-- wow js-->
        <script src="https://sigosoft.com/assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="https://sigosoft.com/assets/js/aos.min.js"></script>
        <!-- main -->
        <script src="https://sigosoft.com/assets/js/main.min.js"></script>
        <!--<script src="assets/js/script.js"></script>-->


    
    
        
        
        <!-- footer end -->

       

    </body>


</html>