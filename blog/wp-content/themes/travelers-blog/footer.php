// <?php

// do_action( 'travelers_blog_footer_top' );

// travelers_blog_get_footer(); ?>

<!-- start Back To Top -->
<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end Back To Top -->

<div class="full-screen-search-box-wrap" style="display: none;">
	<div class="search-box-wrappper">
		<div class="searchform" role="search">
			<a href="javascript:void(0)" class="hide_full_search">
				<i class="fa fa-times" aria-hidden="true"></i>
			</a>
			<?php get_search_form(); ?>
		</div>
	</div>
</div>
<?php include "includes/footer.php";?>


 	<!--	<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.min.js"></script>-->
	    

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/waypoints.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/rs-plugin/js/jquery.themepunch.tools.min.js"></script>-->
		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.animateNumber.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/slick.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.easypiechart.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.prettyPhoto.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.sharrre.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.elevateZoom-3.0.8.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.placeholder.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/twitterfeed.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jflickrfeed.min.js"></script>-->


		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/mailChimp.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.nicescroll.min.js"></script>-->
		

		<!--<script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/script.js"></script>-->
<?php wp_footer(); ?>
</body>
</html>
