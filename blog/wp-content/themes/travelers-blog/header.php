<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Top Mobile App Development Company in USA, New York" />
<meta property="og:description" content="We re the top and leading Mobile app development Company in USA, New York  We provide robust and reliable mobile app development service at an affordable budget.." />
<meta property="og:url" content="https://www.sigosoft.com/blog" />
<meta property="og:site_name" content="Sigosoft" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="We re the top and leading Mobile app development Company in USA, New York  We provide robust and reliable mobile app development service at an affordable budget." />
<meta name="twitter:title" content="Top Mobile App Development Company in USA, New York" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>Mobile App Development Company in USA | Mobile App Developers in USA</title>
<meta content="We re the top and leading Mobile app development Company in USA, New York  We provide robust and reliable mobile app development service at an affordable budget." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	    <!-- favicon -->
    <link rel="shortcut icon" href="https://www.sigosoft.com/assets/img/favicon-sigosoft.webp" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/fonts/flaticon.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/animate.min.css">
    <!-- Owl Carousel -->
    <!-- magnific popup -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/magnific-popup.min.css">
    <!-- aos scoll animation css -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/aos.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/style.min.css">
    <!-- responsive -->
    <link rel="stylesheet" href="https://www.sigosoft.com/assets/css/responsive.min.css">

    <link rel="stylesheet" type="text/css" href="https://www.sigosoft.com/assets/css/custom.css">
	<?php wp_head(); ?>
</head>

<?php

$body_id = travelers_blog_get_body_id(); ?>

<body id="<?php echo esc_attr($body_id); ?>" <?php body_class(); ?>>

	<?php 
	do_action( 'travelers_blog_after_body' );

	if ( ! function_exists( 'wp_body_open' ) ) {
        function wp_body_open() {
            do_action( 'wp_body_open' );
        }
	}
	?>

	<header id="masthead">
<?php include "includes/header.php";?>
		
	</header><!-- header section end -->

	<?php 

	global $template; // For elementor
	if( !is_home() && 
		!is_single() && 
		!is_404() && 
		!is_front_page()
	){
		if( basename($template) != 'header-footer.php' ){
			travelers_blog_breadcrumb();
		}
	}