                <!-- footer begin -->
        <div class="footer footer-2">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between">
                    
                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>About Us</h3>
                            <ul>
                                <li>
                                    <a href="https://www.sigosoft.com/about.php">Our Profile</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/partner-with-us.php">Partner with Us</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/team.php">Our Team</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/careers.php">Careers</a>
                                </li>
                                
                                <li>
                                    <a href="https://www.sigosoft.com/technologies.php">Technologies</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-2 col-lg-2 col-md-4">
                        <div class="links-widget">
                            <h3>Services</h3>
                            <ul>
                                <li>
                                    <a href="https://www.sigosoft.com/mobile-app-development-company-in-calicut-india.php">Mobile App Development</a>
                                </li>
                               <li>
                                    <a href="https://www.sigosoft.com/ecommerce-website-development.php">E-Commerce Development</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/magento-development-company.php">Magento Development</a>
                                </li>
                                
                                
                            </ul>
                        </div>
                    </div>

                    <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="links-widget">
                            <h3>Products</h3>
                            <div class="row">
                            <ul class="col-md-6">
                                <li>
                                    <a href="https://www.sigosoft.com/ecommerce-mobile-app-development.php">E-Commerce Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/supply-chain-mobile-app-development.php">Supply Chain Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/e-learning-mobile-app-development.php">E-Learning Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/telemedicine-app-development.php">Online Consultation Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/loyalty-mobile-app-development.php">Loyalty Apps</a>
                                </li>
                            </ul>
                            <ul class="col-md-6">
                                <li>
                                    <a href="https://www.sigosoft.com/air-ticket-booking-app-for-android-iOS.php">Flight Booking Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/hotel-booking-app-development.php">Hotel Booking Apps</a>
                                </li>

                                <li>
                                    <a href="https://www.sigosoft.com/food-delivery-app-development.php">Food Delivery Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/van-sales-mobile-application.php">Van Sales Apps</a>
                                </li>
                                <li>
                                    <a href="https://www.sigosoft.com/classified-app-development-classified-app-builder.php">Classifieds Apps</a>
                                </li>
                                <!--<li>
                                    <a href="gym-fitness-mobile-app-development-company-in-usa.php">Gym Fitness Apps</a>
                                </li>-->
                                
                            </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-8">
                        <div class="about-widget links-widget">
                            <h3>Contact</h3>
                            <ul>
                                <li><a href="tel:+91 9846237384"><i class="fas fa-mobile-alt"></i>+91 9846237384</a></li>
                                <?php /*<li><a href="https://api.whatsapp.com/send?phone=919846237384"><i class="fab fa-whatsapp"></i> +91 9846237384</a></li> */?>
                                <li><a href="mailto:info@sigosoft.com"><i class="far fa-envelope-open"></i> info@sigosoft.com</a></li>
                                <li style="color: #bdbdbd; font-size: 15px; line-height: 25px;"><i class="fas fa-map-marker-alt"></i>
                                    <span>Calicut, Kerala, India.</span><br>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- footer end -->
        
        
        
        
        
        
            <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f022eb4760b2b560e6fc8b8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
        
        
     <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "name": "Sigosoft",
  "url": "https://www.sigosoft.com",
  "logo": "https://www.sigosoft.com/assets/img/logo-sigosoft.png",
  "sameAs": [
    "https://www.facebook.com/sigosoft",
    "https://twitter.com/sigosoft_social",
    "https://www.instagram.com/sigosoft_/",
    "https://www.youtube.com/channel/UC5y70ytFjNLrYPbSUyUp2Cw",
    "https://www.linkedin.com/company/sigosoft/"
  ]
}
</script>


<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "Sigosoft",
  "image": "https://www.sigosoft.com/assets/img/logo-sigosoft.png",
  "@id": "",
  "url": "https://www.sigosoft.com",
  "telephone": "+91 - 9846237384",
  "priceRange": "$$$$",
  "address": {
    
    "@type": "PostalAddress",
    "streetAddress": "India, Kerala",
    "addressLocality": "Calicut",
    "postalCode": "673019",
    "addressCountry": "IN"
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "00:00",
    "closes": "23:59"
  } 
}
</script>


   
        
         <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-176910320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-176910320-1');
</script>
        
        

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="badges-wrapper text-center">
                            <ul class="badges">
                                <li><a href="#" target="_blank"><img src="https://www.sigosoft.com/assets/img/badges/1.png" alt="Mobile App Development Company in Kerala"></a></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/2.png" alt="Mobile App Development Company in Calicut"></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/3.png" alt=""></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/4.png" alt=""></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/5.png" alt=""></li>
                                <li><img src="https://www.sigosoft.com/assets/img/badges/6.png" alt=""></li>
                                <li><a href="#" target="_blank"><img src="https://www.sigosoft.com/assets/img/badges/8.png" alt="Mobile App Development Company in India"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- copyright begin -->
        <div class="copyright">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2014-2020 Sigosoft. All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                                
                                <li>
                                    <a class="facebook" href= https://www.facebook.com/sigosoft><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a class="instagram" href=https://www.instagram.com/sigosoft_><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="https://twitter.com/sigosoft_social"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href=https://join.skype.com/invite/Qq1GmNOs9DDV><i class="fab fa-skype"></i></a>
                                </li>
                                 <li>
                                  <a class="facebook" href= https://www.linkedin.com/company/sigosoft/><i class="fab fa-linkedin"></i></a>
                                </li>
                                
                              
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- copyright end -->

        <div class="whatsapp">
            <a href="https://api.whatsapp.com/send?phone=919846237384" target="_blank"><img src="https://sigosoft.com/assets/img/whatsapp.png" alt="whatsapp"/></a>
        </div>
        
        
            <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f022eb4760b2b560e6fc8b8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
            <!-- jquery -->
        <script src="https://sigosoft.com/assets/js/jquery-3.4.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
        <!-- owl carousel -->
        <script src="https://sigosoft.com/assets/js/owl.carousel.min.js"></script>
        <!-- magnific popup -->
        <script src="https://sigosoft.com/assets/js/jquery.magnific-popup.min.js"></script>
        <!-- counter up js -->
        <script src="https://sigosoft.com/assets/js/jquery.counterup.min.js"></script>
        <script src="https://sigosoft.com/assets/js/counter-us-activate.min.js"></script>
        <!-- waypoints js-->
        <script src="https://sigosoft.com/assets/js/jquery.waypoints.min.js"></script>
        <!-- wow js-->
        <script src="https://sigosoft.com/assets/js/wow.min.js"></script>
        <!-- aos js -->
        <script src="https://sigosoft.com/assets/js/aos.min.js"></script>
        <!-- main -->
        <script src="https://sigosoft.com/assets/js/main.min.js"></script>
        <!--<script src="assets/js/script.js"></script>-->


    
    
        
        
        <!-- footer end -->

       