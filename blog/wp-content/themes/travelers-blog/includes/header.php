<!-- preloader begin -->
        <div class="preloader">
            <div id="circle_square">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- preloader end -->
<!-- header begin -->
        <div class="header-2">
            <div class="topbar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                            <p class="welcome-text">We provide worldwide services 24/7</p>
                        </div>
                        <div class="col-xl-6 col-lg-6">
                            <div class="support-bars">
                                <ul>
                                    <li>
                                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                                        info@sigosoft.com
                                    </li>
                                    <li>
                                        <span class="icon"><i class="fas fa-phone"></i></span>
                                       +91 9846237384
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottombar">
                <div class="container this-container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-7 d-xl-block d-lg-block d-flex align-items-center">
                                    <div class="logo">
                                        <a href="https://www.sigosoft.com">
                                            <img src="https://www.sigosoft.com/assets/img/logo-sigosoft.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-5 d-xl-none d-lg-none d-block">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-9">
                            <div class="mainmenu">
                                <nav class="navbar navbar-expand-lg">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                        <ul class="navbar-nav ml-auto">
                                            
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com">Home</a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        About
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/about">Our Profile</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/team">Our Team</a>                                                    
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/technologies">Our Technologies</a>
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/partner-with-us">Partner with Us</a>
                                                
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Services
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                                    <a class="dropdown-item" href="https://www.sigosoft.com/mobile-app-development-company-in-calicut-india">Mobile App Consultation</a>
<a class="dropdown-item" href="https://www.sigosoft.com/android-app-development">Native Android App Development</a>
<a class="dropdown-item" href="https://www.sigosoft.com/ios-app-development">Native iOS App Development</a>
<a class="dropdown-item" href="https://www.sigosoft.com/flutter-app-development">Flutter App Development</a>
<a class="dropdown-item" href="https://www.sigosoft.com/react-native-development">React Native Development</a>
<a class="dropdown-item" href="https://www.sigosoft.com/mobile-app-ui-designing">Mobile App UI Designing</a>
<a class="dropdown-item" href="https://www.sigosoft.com/mobile-app-testing">Mobile App Testing</a>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Products
                                                </a>
                                                <div class="dropdown-menu dmw500" aria-labelledby="navbarDropdown4">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
           <a class="dropdown-item" href="https://www.sigosoft.com/ecommerce-mobile-app-development">E-Commerce Apps</a>
           <a class="dropdown-item" href="https://www.sigosoft.com/loyalty-mobile-app-development">Loyalty Apps</a>
         <a class="dropdown-item" href="https://www.sigosoft.com/e-learning-mobile-app-development">E-Learning Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/community-mobile-app-development">Community Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/supply-chain-mobile-app-development">Supply Chain Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/rent-a-car-mobile-app">Rent a Car Apps</a>
      <a class="dropdown-item" href="https://www.sigosoft.com/telemedicine-app-development">TeleMedicine  Apps</a>
     
   </div>
   <div class="col-md-6">
      <a class="dropdown-item" href="https://www.sigosoft.com/food-delivery-app-development">Food Delivery Apps</a>
     <a class="dropdown-item" href="https://www.sigosoft.com/classified-app-development-classified-app-builder">Classifieds Apps</a>
     <a class="dropdown-item" href="https://www.sigosoft.com/air-ticket-booking-app-for-android-iOS">Flight Booking Apps</a>
   <a class="dropdown-item" href="https://www.sigosoft.com/hotel-booking-app-development">Hotel Booking Apps</a>
  <!-- <a class="dropdown-item" href="https://www.sigosoft.com/gym-fitness-mobile-app-development">Gym & Fitness Apps</a>-->
   <a class="dropdown-item" href="https://www.sigosoft.com/sports-app-development-company">Sports Apps</a>
    <a class="dropdown-item" href="https://www.sigosoft.com/car-wash-app-development">Car Wash Apps</a>
   <a class="dropdown-item" href="https://www.sigosoft.com/van-sales-mobile-application">Van sales Apps</a>
   
   </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/portfolio">Portfolio</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/blog">Blogs</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/careers">Careers</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="https://www.sigosoft.com/contact">Contact</a>
                                            </li>
                                        </ul>
                                        <div class="header-buttons">
                                <a href="https://www.sigosoft.com/contact" class="quote-button">Get A Quote</a>
                            </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 <style>
 body{
     background:#f2f2f2;
 }
 header{
     background:#fff;
 }
 .list-item{
    background:#fff;
    margin-bottom: 15px;
 }
 #sidebar .widget{
     background:#fff;
    margin-bottom: 15px;
 }
 #home_banner_blog{
     margin-bottom: 15px;
 }
 .container {
    width: 100%;
 }
 .blog {
    padding: 0;
}
                    .header-2 .bottombar .header-buttons {
    width: auto !important;
}
.dmw500{
    width:500px;
}
.banner-2 .banner-content {
    padding: 80px 0;
}
.port{
    background: -webkit-linear-gradient(21deg,#d63438 0,#90191c 100%) !important;
    color: #fff !important;
    height: 50px;
    margin-top: 20px !important;
    padding: 14px !important;
    border-radius: 3px;
}

.header-2 .bottombar .mainmenu .navbar .navbar-nav .nav-item .nav-link.port:before{
      background: #0000;
}
.navbar{
    margin-bottom: 0px;
}
@media only screen and (max-width: 768px){
    .port{
    margin-top: auto !important;
    }
}
                </style>
       <!-- header end -->