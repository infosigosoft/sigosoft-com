
<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Careers| Sigosoft "/>
<meta property="og:description" content="Get the best job opportunities for your bright careers in programming, android, iOS, app designing and development only at Sigosoft."/>
<meta property="og:url" content="https://www.sigosoft.com/careers.php"/>
<meta property="og:site_name" content="Sigosoft"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="Get the best job opportunities for your bright careers in programming, android, iOS, app designing and development only at Sigosoft. "/>
<meta name="twitter:title" content="Get the best job opportunities for your bright careers in programming, android, iOS, app designing and development only at Sigosoft" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>Careers| Sigosoft USA, New York</title>
<meta content="Get the best job opportunities for your bright careers in programming, android, iOS, app designing and development only at Sigosoft." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>



        <?php include('header.php'); ?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-careers">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Careers</h2>
                            <ul>
                                <li><a href=".">Home</a></li>
                                <li>Careers</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- about begin -->
        <div class="about-page-about text-center">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="part-text">

                            <h2>We are <span class="special">Hiring</span></h2>
                            <p>Sigosoft's strong focus on technology innovation provides a wide range of opportunities for organic career growth. More importantly, the focus is on work-life balance along with diversity and inclusivity at all levels of the organization. Retaining talent is an area of focus as we believe that our talent is our greatest strength. Sigosoft provides exciting career opportunities for each individual desiring to enhance his professional growth. At Sigosoft we value our talented team and our compensation package reflects this. Our benefits are flexible and far-reaching in scope.</p>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- about end -->

        <!-- contact begin -->
        <div class="careers pb-5">
            <div class="container">
                <div class="row justify-content-around">
                    <div class="col-xl-6 col-lg-6">
                        <div id="accordion" class="accordion-careers">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  01. MAGENTO DEVELOPER
                                    </button>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Skills Required:</span> </p>
                                <ul>
                                    <li><i  class="fas fa-check-square"></i> PHP 5, MYSQL (Complex database designing experience/knowledge), AJAX, JQUERY</li>
                                    <li><i  class="fas fa-check-square"></i> PHP Frameworks: Magento</li>
                                    <li><i  class="fas fa-check-square"></i> Good command over written and verbal English communication.</li>
                                    <li><i  class="fas fa-check-square"></i> Understanding of Business Requirement.</li>
                                    <li><i  class="fas fa-check-square"></i> Good knowledge about SDLC.</li>
                                    <li><i  class="fas fa-check-square"></i> Good team player.</li>
                                </ul>
                                <p><span class="reqmt-title">Experience:</span> 1+ years</p> 
                                <p><span class="reqmt-title">Employment Type:</span> Full Time</p>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingTwo">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  02. IOS DEVELOPER
                                </button>
                              </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Skills Required:</span> </p>
                                <ul>
                                    <li><i  class="fas fa-check-square"></i> Swift, Xcode, Reactive UI, Use of SourceTree, BitBucket, RxSwift (Preferred), CocoaPods, AutoLayout, MVC/MVVM</li>
                                    <li><i  class="fas fa-check-square"></i> Good command over written and verbal English communication.</li>
                                    <li><i  class="fas fa-check-square"></i> Understanding of Business Requirement.</li>
                                    <li><i  class="fas fa-check-square"></i> Good knowledge about SDLC.</li>
                                    <li><i  class="fas fa-check-square"></i> Good team player.</li>
                                </ul>
                                
                              </div>
                            </div>
                          </div>
                          
                          
                          <div class="card">
                            <div class="card-header" id="headingThree">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  03. REACT NATIVE DEVELOPER
                                </button>
                              </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Skills Required:</span> </p>
                                <ul>
                                  
                                    
                               <li><i  class="fas fa-check-square"></i> React native application developer, proficient in coding debugging & unit testing.</li>

                                <li><i  class="fas fa-check-square"></i>Good understanding of android ios web design guidelines, SDK and excellent javascript skills.</li>

                               <li><i  class="fas fa-check-square"></i> Minimum 2 years experience in react native development.</li>

                               <li><i  class="fas fa-check-square"></i>Knowledge in Hooks, npm or yarn, git, vs code.</li>

                               <li><i  class="fas fa-check-square"></i>Concepts of native bridging and native modules in react native.</li>

                               <li><i  class="fas fa-check-square"></i>Solid at working with third-party component and debugging dependency conflicts.</li>

                               <li><i  class="fas fa-check-square"></i> Experience in native development for ios and android.</li>

                               <li><i  class="fas fa-check-square"></i>Understanding of rest APIs, the document request model, and offline storage.</li>

                               <li><i  class="fas fa-check-square"></i>Excellent communication skills, and good team player.</li>
                                     </ul>
                                
                              </div>
                            </div>
                          </div>
                          
                          
                          <div class="card">
                            <div class="card-header" id="headingFour">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                  04. FLUTTER DEVELOPER
                                </button>
                              </h5>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                              <div class="card-body">
                                <p><span class="reqmt-title">Key Requirements:</span> </p>
                                <ul>
                                 
                                   <li><i  class="fas fa-check-square"></i> Must have Worked with Flutter / Android Application Development.</li>
                                   <li><i  class="fas fa-check-square"></i> Excellent Ability to Develop & Understand Algorithms.</li>
                                   <li><i  class="fas fa-check-square"></i> Strong Knowledge in Restful Web Services. Experience in Interacting with Web.</li>
                                   <li><i  class="fas fa-check-square"></i> Experience with Offline Storage, Threading, and Performance Tuning.</li>
                                   <li><i  class="fas fa-check-square"></i> Strong Knowledge in Push Notifications.</li>
                                   <li><i  class="fas fa-check-square"></i> Strong Knowledge in API Integrations.</li>
                                   <li><i  class="fas fa-check-square"></i> Analytical, Problem Solving and Decision Making Skills.</li>
                                    
                                    
                                </ul>
                                
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6">
                        <div class="career-form">
                            <h4>Apply for a Job</h4>
                            <form id="career-form" method="post" action="send-career.php" enctype="multipart/form-data">
                                <input name="name" type="text" placeholder="Full Name" onkeypress='return ((event.charCode >= 65 && event.charCode <= 90) || (event.charCode >= 97 && event.charCode <= 122) || (event.charCode == 32))' maxlength="50" required>
                                <input name="email" type="email" placeholder="Email" maxlength="50" required>
                                <select name="position" required>
                                    <option value="Magento Developer">Magento Developer</option>
                                    <option value="iOS Developer">iOS Developer</option>
                                    <option value="Flutter Developer">Flutter Developer</option>
                                    <option value="React Native Developer">React Native Developer</option>
                                </select>
                                <input type="file" name="resume" id="resume" accept=".docx,.doc,.pdf" required>
                                <p class="file-error" style="color: #f00; display: none;">Max file size is 2MB</p>
                                <textarea name="message" placeholder="Message" required></textarea>
                                <div class="g-recaptcha" id="rcaptcha"  data-sitekey="6LcB5NoUAAAAAJcqpoCY66QDw3ZspO_DuQ-7EupH"></div>
                                <span id="captcha" style="color: #f00;" /><br>
                                <button type="submit" name="career_submit" class="btn-murtes-6">Submit Now <i class="fas fa-long-arrow-alt-right"></i></button>
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- contact end -->

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
        <script type="text/javascript">
            $('#resume').bind('change', function(){
                //alert(this.files[0].size);
                if(this.files[0].size > 2097152) {
                    $(this).val('');
                    $('.file-error').css("display","block");
                }
                else {
                    $('.file-error').css("display","none");
                }

            });
        </script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
          
           $('form').on('submit', function(e) {
              if(grecaptcha.getResponse() == "") {
                e.preventDefault();
                document.getElementById('captcha').innerHTML="You can't leave Captcha Code empty";
                return false;
              } else {
                // alert("Thank you for requesting a Quotation, we will reach back shortly...");
                 return true; 
              }
            });
        </script>
    </body>


</html>