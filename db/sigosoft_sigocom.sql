-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 21, 2021 at 05:56 PM
-- Server version: 10.3.20-MariaDB-cll-lve
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sigosoft_sigocom`
--

-- --------------------------------------------------------

--
-- Table structure for table `career`
--

CREATE TABLE `career` (
  `CareerID` int(11) NOT NULL,
  `CareerFullName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CareerEmail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CareerPosition` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CareerResume` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CareerMessage` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CareerDateTime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `career`
--

INSERT INTO `career` (`CareerID`, `CareerFullName`, `CareerEmail`, `CareerPosition`, `CareerResume`, `CareerMessage`, `CareerDateTime`) VALUES
(1, 'Evan', 'evan@test.com', 'Magento Developer', 'Logo 3.pdf', 'test', '2020-08-28 08:40:17'),
(2, 'Malu', 'malu@gmail.com', 'Magento Developer', 'TEST PDF.pdf', 'Just test', '2020-09-02 01:53:34'),
(3, 'test', 'test@gmil.com', 'Magento Developer', 'TEST PDF.pdf', 'tessttt', '2020-09-02 09:22:58'),
(4, 'M Abhishek', 'abhishek1995m@gmail.com', 'Software Tester', 'Resume1.pdf', 'I have done the course in Software Testing Advanced level and can assure that I can grasp things quickly.', '2020-09-21 06:26:01'),
(5, 'SACHIN S', 'sachinsachu3355@gmail.com', 'Software Tester', 'Sachin_Resume.pdf', '2.3 Year of Experience in Software Testing (Manual & Automation). ', '2020-09-24 15:50:08'),
(6, 'Anjana K M', 'anjuanjana96@gmail.com', 'Software Tester', 'TestingResume.docx', 'Please consider the resume attached for the post of software tester', '2020-10-03 13:50:42'),
(7, 'test one', 'one@test.com', 'Magento Developer', 'TEST PDF.pdf', 'test message', '2020-10-16 09:55:47'),
(8, 'test one', 'one@test.com', 'Magento Developer', 'TEST PDF.pdf', 'test message', '2020-10-16 09:57:10'),
(9, 'test one', 'one@test.com', 'Magento Developer', 'TEST PDF.pdf', 'test message', '2020-10-16 10:16:17'),
(10, 'Apu Sadanand P', 'apuparaparambil@gmail.com', 'Magento Developer', 'ST_Resume_Apu(cer).pdf', 'For the post of software tester', '2020-10-22 04:55:09'),
(11, 'Akshaya P', 'akshayaakku07@gmail.com', 'iOS Developer', 'Resme-Akshaya P .pdf', 'I have recently graduated with a B.Tech in Computer Science and Engineering from Vedavyasa Institute of Technology. \r\nI have attached my resume for your consideration. Please take a moment to go through them to get a better picture of who I am. It would give me great pleasure to hear back from you regarding my application.', '2020-11-24 06:54:59'),
(12, 'Sundharam Gunasekaran', 'sundharamsriram001@gmail.com', 'Flutter Developer', 'Sundharam_G_Projects.docx', 'Enclosed you will find my rÃ©sumÃ© and application materials for the Freshers position that is currently open at this organisation. I recently completed my B.E. in Electronics and Communication Engineering.\r\n\r\n \r\n\r\nAm Fresher. Am complete my graduation in 2020 only. In the past 3 years,  I have Experienced in Entrepreneurship. I had a startup in Coimbatore but I closed by some mistakes I did.  This is a new field for me. but   I have teaching Experience in photoshop,  C, C++, Java, Python and SQ', '2020-12-20 10:07:33'),
(13, 'Sooraj Dev', 'soorajdev93@gmail.com', 'Flutter Developer', 'Resume202012180803.pdf', 'Hi,\r\nI have around 5.7 year of experiance in development and i have experiance in flutter is around 1.5 year and 5 year in angular and 2 year in nodejs', '2021-01-12 03:36:43'),
(14, 'nithya km', 'nithyakm90@gmail.com', 'Magento Developer', 'Nithya_HR Profile.docx', 'HR Role', '2021-03-01 10:43:39'),
(15, 'jo', 'jo@gmail.com', 'Magento Developer', 'TEST PDF.pdf', 'TEST', '2021-04-01 03:43:39'),
(16, 'Jo', 'jo@gmail.com', 'Magento Developer', 'TEST PDF.pdf', 'Test', '2021-04-01 03:53:41'),
(17, 'Jo', 'jo@gmail.com', 'Magento Developer', 'TEST PDF.pdf', 'Test', '2021-04-01 03:53:59');

-- --------------------------------------------------------

--
-- Table structure for table `careers`
--

CREATE TABLE `careers` (
  `cid` int(11) NOT NULL,
  `post` varchar(100) NOT NULL,
  `descr` text NOT NULL,
  `ldate` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers`
--

INSERT INTO `careers` (`cid`, `post`, `descr`, `ldate`) VALUES
(22, 'Magento Developer', 'Skills Required PHP 5 MYSQL (Complex database designing experience/knowledge) AJAX, JQUERY PHP Frameworks: Magento Good command over written and verbal English communication Understanding of Business Requirement Good knowledge about SDLC Good team player. Experience: 1+ years EmploymentType:Full Time', '2019-10-31'),
(20, 'iOS Developer', 'Swift, Xcode, Reactive UI, Use of SourceTree, BitBucket, RxSwift(Preferred), CocoaPods, AutoLayout, MVC/MVVM', '2019-10-31'),
(21, 'Software Tester', 'Key Responsibilities: Should have hands on experience in test planning, test analysis, test design, construction and verification, testing cycles etc. Should be experienced in white box, black box, unit testing, integration testing, system testing, compatibility testing, regressing testing. Experience in Appium, Automation tools will be added advantage. Should be able to work with developers to investigate and fix issue and verify the fixes. Good spoken and written English will be an advantage.', '2019-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `consultation`
--

CREATE TABLE `consultation` (
  `c_id` int(11) NOT NULL,
  `emails` varchar(100) DEFAULT NULL,
  `c_date` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `consultation`
--

INSERT INTO `consultation` (`c_id`, `emails`, `c_date`) VALUES
(2, 'shyamily@sigosoft.com', '2018-12-20'),
(12, 'techzz.vipin@gmail.com', '2018-12-29'),
(11, 'zssss@gmail.com', '2018-12-29'),
(10, 'akhilkrishna1920@gmail.com', '2018-12-21'),
(9, 'aithinep@gmail.com', '2018-12-20'),
(8, 'swathi@sigosoft.com', '2018-12-20'),
(13, 'a@bi.c', '2018-12-31'),
(14, 'a@bi.c', '2018-12-31'),
(15, 'Shahbasci@gmail.com', '2019-01-01'),
(16, 'pk097503988@gmail.com', '2019-01-02'),
(17, 'ghghgb@gmail.com', '2019-01-06'),
(18, 'asishs123@gmail.com', '2019-01-06'),
(19, 'fghhh@gmail.com', '2019-01-06'),
(20, 'tajhm6724@gmail.com', '2019-06-06'),
(21, 'kimglaze30@gmail.com', '2019-06-06'),
(22, 'tajhm6724@gmail.com', '2019-06-06'),
(23, 'bahammond1@aol.com', '2019-06-06'),
(24, 'brandon.lesniak@gmail.com', '2019-06-06'),
(25, 'janelle2891@gmail.com', '2019-06-06'),
(26, 'biondog@sutterhealth.org', '2019-06-07'),
(27, 'reinhold.wartbichler@gmail.com', '2019-06-07'),
(28, 'tmhwhite1@yahoo.com', '2019-06-07'),
(29, 'bgkeeble@gmail.com', '2019-06-07'),
(30, 'caseymour@nycap.rr.com', '2019-06-07'),
(31, 'salluvsyu@aol.com', '2019-06-07'),
(32, 'jacksonrobic@yahoo.com', '2019-06-07'),
(33, 'davenicoll@cox.net', '2019-06-07'),
(34, 'mbrigdon88@gmail.com', '2019-06-07'),
(35, 'salluvsyu@aol.com', '2019-06-07'),
(36, 'whelanmurphy@aol.com', '2019-06-07'),
(37, 'monica.yap2012@gmail.com', '2019-06-08'),
(38, 'jmgorman@gmail.com', '2019-06-08'),
(39, 'salluvsyu@aol.com', '2019-06-08'),
(40, 'rpanor@yahoo.com', '2019-06-08'),
(41, 'reneeallgeo@gmail.com', '2019-06-08'),
(42, 'mrs.pechal@gmail.com', '2019-06-08'),
(43, 'digvir.jayas@umanitoba.ca', '2019-06-08'),
(44, 'sarahjanej.3283@gmail.com', '2019-06-09'),
(45, 'rogueleader8740@sbcglobal.net', '2019-06-09'),
(46, 'tvraimo@gmail.com', '2019-06-09'),
(47, 'christopher.estay@gmail.com', '2019-06-09'),
(48, 'jasonmartin926@gmail.com', '2019-06-09'),
(49, 'jmmurray4@aol.com', '2019-06-10'),
(50, 'barton1700@gmail.com', '2019-06-10'),
(51, 'nnharper@yahoo.com', '2019-06-10'),
(52, 'njanani437@gmail.com', '2019-06-10'),
(53, 'martina.spackova@gmail.com', '2019-06-10'),
(54, 'ctina1904@gmail.com', '2019-06-10'),
(55, 'simon.ernst@gmail.com', '2019-06-10'),
(56, 'pheajean@gmail.com', '2019-06-10'),
(57, 'ztwalcott@sonic.net', '2019-06-11'),
(58, 'sarahjanej.3283@gmail.com', '2019-06-11'),
(59, 'michael.w.seto@gmail.com', '2019-06-11'),
(60, 'office@immigrate.expert', '2019-06-11'),
(61, 'holly.l.marie@gmail.com', '2019-06-11'),
(62, 'sumitgharal@gmail.com', '2019-06-11'),
(63, 'ruby.lidia@gmail.com', '2019-06-12'),
(64, 'mdesigngroup@gmail.com', '2019-06-12'),
(65, 'sahag.ltg@gmail.com', '2019-06-12'),
(66, 'bimphotography@yahoo.com', '2019-06-12'),
(67, 'ivone.munoz0530@gmail.com', '2019-06-12'),
(68, 'natpeck@gmail.com', '2019-06-12'),
(69, 'tiffanyharker1@yahoo.com', '2019-06-12'),
(70, 'tiffanyharker1@yahoo.com', '2019-06-12'),
(71, 'keevinwilliams@gmail.com', '2019-06-12'),
(72, 'patricpera@yahoo.com', '2019-06-14'),
(73, 'rodney.stigall@gmail.com', '2019-06-14'),
(74, 'jenniewangit@gmail.com', '2019-06-14'),
(75, 'shamilton01@gmail.com', '2019-06-14'),
(76, 'hc10lac@gmail.com', '2019-06-14'),
(77, 'mark_billiedykes@yahoo.com', '2019-06-14'),
(78, 'margot.lorenz@gmail.com', '2019-06-14'),
(79, 'jenny.tricitylawncare@gmail.com', '2019-06-14'),
(80, 'margot.lorenz@gmail.com', '2019-06-14'),
(81, 'stephanie.schumm28@gmail.com', '2019-06-14'),
(82, 'keelylayne87@gmail.com', '2019-06-14'),
(83, 'alex.dworetz@vcuhealth.org', '2019-06-14'),
(84, 'gisellakestler@gmail.com', '2019-06-14'),
(85, 'kimberlywebb93@yahoo.com', '2019-06-14'),
(86, 'fowlerbb@gmail.com', '2019-06-14'),
(87, 'pamjue@gmail.com', '2019-06-16'),
(88, 'rudolphis@charter.net', '2019-06-16'),
(89, 'pollokst@gmail.com', '2019-06-16'),
(90, 'jenny.fletcher9@gmail.com', '2019-06-16'),
(91, 'joaquimsaneto@gmail.com', '2019-06-17'),
(92, 'srshaffer1971@yahoo.com', '2019-06-17'),
(93, 'toly.stoikos@nab.com.au', '2019-06-17'),
(94, 'jennandbryhinson@gmail.com', '2019-06-17'),
(95, 'jennandbryhinson@gmail.com', '2019-06-17'),
(96, 'jcarolineh@yahoo.com', '2019-06-17'),
(97, 'antoinette.sanwidi@free.fr', '2019-06-17'),
(98, 'snoonan1993@gmail.com', '2019-06-17'),
(99, 'sflores7441@gmail.com', '2019-06-17'),
(100, 'mikewinkler1949@gmail.com', '2019-06-17'),
(101, 'anna.m.ngo@gmail.com', '2019-06-18'),
(102, 'ian.michael.martin@gmail.com', '2019-06-18'),
(103, 'rodhicks@yahoo.com', '2019-06-18'),
(104, 'dmsmith1949@aol.com', '2019-06-18'),
(105, 'ichiehwu@gmail.com', '2019-06-18'),
(106, 'rhea.boysen@gmail.com', '2019-06-18'),
(107, 'sjhudek@gmail.com', '2019-06-18'),
(108, 'dcanaday0989@gmail.com', '2019-06-18'),
(109, 'sgtcakeman@gmail.com', '2019-06-18'),
(110, 'brett@richmanlawkc.com', '2019-06-18'),
(111, 'andik9@inbox.lv', '2019-06-18'),
(112, 'asmclemore@gmail.com', '2019-06-18'),
(113, 'clarks082013@gmail.com', '2019-06-18'),
(114, 'matt.dg.wright@gmail.com', '2019-06-19'),
(115, 'brianthemitchell@gmail.com', '2019-06-19'),
(116, 'chenfeng112001@gmail.com', '2019-06-19'),
(117, 'jenriquehf@gmail.com', '2019-06-19'),
(118, 'nreilly3@gmail.com', '2019-06-19'),
(119, 'timothyhansen02@gmail.com', '2019-06-19'),
(120, 'saragerstenblatt@gmail.com', '2019-06-19'),
(121, 'videogameplayerperson@gmail.com', '2019-06-19'),
(122, 'jw.redrose@aol.com', '2019-06-20'),
(123, 'silber175@gmail.com', '2019-06-20'),
(124, 'brentbaber@gmail.com', '2019-06-20'),
(125, 'andrewrussellcanoe@gmail.com', '2019-06-20'),
(126, 'jeanmclaughlin19@yahoo.com', '2019-06-20'),
(127, 'simon@formbytoolhire.co.uk', '2019-06-20'),
(128, 'vicky.moyer@yahoo.com', '2019-06-20'),
(129, 'janada30@yahoo.com', '2019-06-20'),
(130, 'ysupangkat@yahoo.com', '2019-06-20'),
(131, 'hrrychan@yahoo.com', '2019-06-20'),
(132, 'doloreshansen@aol.com', '2019-06-20'),
(133, 'tommomcrief@gmail.com', '2019-06-20'),
(134, 'doloreshansen@aol.com', '2019-06-20'),
(135, 'mfalcon41@gmail.com', '2019-06-20'),
(136, 'asmani.patel@gmail.com', '2019-06-20'),
(137, 'regbrown@yahoo.com', '2019-06-20'),
(138, 'brendon.kendrick@yahoo.com', '2019-06-20'),
(139, 'han.juyoun@gmail.com', '2019-06-20'),
(140, 'alexandra.gendron68@gmail.com', '2019-06-20'),
(141, 'gjdeno88@gmail.com', '2019-06-20'),
(142, 'jw.redrose@aol.com', '2019-06-21'),
(143, 'gmsroseville@gmail.com', '2019-06-21'),
(144, 'jordanthompson116@gmail.com', '2019-06-21'),
(145, 'audiobirchtree@gmail.com', '2019-06-21'),
(146, 'mjevp3@sbcglobal.net', '2019-06-21'),
(147, 'swanston03@gmail.com', '2019-06-21'),
(148, 'autumnflowers1870@gmail.com', '2019-06-21'),
(149, 'bomclot@yahoo.com', '2019-06-21'),
(150, 'sambh03@gmail.com', '2019-06-21'),
(151, 'mbillie1@gmail.com', '2019-06-21'),
(152, 'gardnergreg12@gmail.com', '2019-06-21'),
(153, 'verakara45@gmail.com', '2019-06-21'),
(154, 'brianthemitchell@gmail.com', '2019-06-21'),
(155, 'pappuhamy@yahoo.com', '2019-06-21'),
(156, 'master1476@gmail.com', '2019-06-21'),
(157, 'stephenlunnyusmc@yahoo.com', '2019-06-21'),
(158, 'ken.darmer@gmail.com', '2019-06-21'),
(159, 'verakara45@gmail.com', '2019-06-21'),
(160, 'emg7.gar@gmail.com', '2019-06-21'),
(161, 'isykleb@gmail.com', '2019-06-21'),
(162, 'bbrazil7@yahoo.com', '2019-06-22'),
(163, 'bhumberson@gmail.com', '2019-06-22'),
(164, 'dhossfeld@sagewater.com', '2019-06-22'),
(165, 'wolfgang.bonsen@email.de', '2019-06-22'),
(166, 'kathylaneross@yahoo.com', '2019-06-22'),
(167, 'jromine1993@gmail.com', '2019-06-22'),
(168, 'dekeirons@yahoo.com', '2019-06-22'),
(169, 'vw1463@yahoo.com', '2019-06-22'),
(170, 'djantonalmazov@gmail.com', '2019-06-22'),
(171, 'garycohn1@gmail.com', '2019-06-22'),
(172, 'wlkthree@gmail.com', '2019-06-22'),
(173, 'ida.hansen@gmail.com', '2019-06-23'),
(174, 'fabulousdj@foxmail.com', '2019-06-23'),
(175, 'luischavira14@gmail.com', '2019-06-23'),
(176, 'bradbw1@gmail.com', '2019-06-23'),
(177, 'ethanan02@gmail.com', '2019-06-23'),
(178, 'addisongloss23@gmail.com', '2019-06-23'),
(179, 'sdgblandon@gmail.com', '2019-06-23'),
(180, 'zoompatriot@gmail.com', '2019-06-23'),
(181, 'pakman1230@gmail.com', '2019-06-23'),
(182, 'ssances@gmail.com', '2019-06-23'),
(183, 'aurelie.gadea.mail@gmail.com', '2019-06-23'),
(184, 'alix.olson52@gmail.com', '2019-06-23'),
(185, 'dfortunato@yahoo.com', '2019-06-24'),
(186, 'hazyczengeni@gmail.com', '2019-06-24'),
(187, 'ckronemeyer2@gmail.com', '2019-06-24'),
(188, 'talmilburn@gmail.com', '2019-06-24'),
(189, 'callumgreenan@gmail.com', '2019-06-24'),
(190, 'lauraprz21@gmail.com', '2019-06-24'),
(191, 'gnrltsao@gmail.com', '2019-06-24'),
(192, 'cingangel@yahoo.com', '2019-06-24'),
(193, 'cestewart1269@gmail.com', '2019-06-24'),
(194, 'yehlethan1@gmail.com', '2019-06-24'),
(195, 'rm237props@gmail.com', '2019-06-24'),
(196, 'j.persaud1791@gmail.com', '2019-07-05'),
(197, 'scott@cavasco.com', '2019-07-06'),
(198, 'pengqiang2012@gmail.com', '2019-07-06'),
(199, 'sethbla@aol.com', '2019-07-06'),
(200, 'vincent.fagot.bouquet@gmail.com', '2019-07-06'),
(201, 'gwarren483@aol.com', '2019-07-06'),
(202, 'bassomatik@gmail.com', '2019-07-07'),
(203, 'terriann.kuster@gmail.com', '2019-07-08'),
(204, 'plumcreek2003@yahoo.com', '2019-07-08'),
(205, 'canedow5254@yahoo.com', '2019-07-23');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `ContactID` int(11) NOT NULL,
  `ContactName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ContactEmail` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ContactMobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ContactMessage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ContactFrom` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ContactDateTime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`ContactID`, `ContactName`, `ContactEmail`, `ContactMobile`, `ContactMessage`, `ContactFrom`, `ContactDateTime`) VALUES
(40, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'This is a message for testing mail. ', '', '2021-02-05 07:20:18'),
(41, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'This is a test message for testing db from uk site....', '', '2021-02-09 01:30:04'),
(42, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'This is a test message from ae site', '', '2021-02-09 01:39:41'),
(43, 'Testcom', 'test.com@gmail.com', '474854444', 'Test', '', '2021-02-09 02:09:43'),
(44, 'Testae', 'Test.ae@gmail.com', '47474746845452', 'Test', '', '2021-02-09 02:10:31'),
(10, 'Frank Lewis', 'frankrklewis@gmail.com', '9330442191', 'Hello,\r\n \r\nHow are you? Hope you are fine.\r\n \r\nI have been checking your website quite often. It has seen that the main keywords are still not in top 10 rank. You know things of working; I mean the procedure of working has changed a lot.\r\n \r\nSo I would like to have opportunity to work for you and this time we will bring the keywords to the top 10 spot with guaranteed period.\r\n \r\nThere is no wondering that it is possible now cause, I have found out that there are few things need to be done for better performances (Some we Discuss ,in this email). Let me tell you some of them -\r\n \r\n1. Title Tag Optimization\r\n2. Meta Tag Optimization (Description, keyword and etc)\r\n3. Heading Tags Optimization\r\n4. Targeted keywords are not placed into tags\r\n5. Alt / Image tags Optimization\r\n6. Google Publisher is missing\r\n7. Custom 404 Page is missing\r\n8. The Products are not following Structured markup data\r\n9. Website Speed Development (Both Mobile and Desktop )\r\n10.Off â€“Page SEO work\r\n \r\nLots are pendingâ€¦â€¦â€¦â€¦â€¦..\r\n \r\nYou can see these are the things that need to be done properly to make the keywords others to get into the top 10 spot in Google Search & your sales Increase.\r\n \r\n \r\nSir/ Madam, please give us a chance to fix these errors and we will give you rank on these keywords.\r\n \r\nPlease let me know if you encounter any problems or if there is anything you need. If this email has reached you by mistake or if you do not wish to take advantage of this advertising opportunity, please accept my apology for any inconvenience caused and rest assured that you will not be contacted again.\r\n \r\nMany thanks for your time and consideration,\r\n \r\nLooking forward\r\n \r\nRegards\r\n\r\nFrank Lewis\r\n\r\n          If you did not wish to receive this, please reply with \"unsubscribe\" in the subject line', '', '2020-09-09 05:44:29'),
(11, 'Bethany Smith', 'bethany@seopackagesprice.com', '5712007758', 'Hi,\r\n\r\nI found your website from a listing directory and observed that despite having a good design, your website still needs the best search engine optimization practice to get higher rankings on Google. I was wondering if you would be interested in optimizing your website to increase organic traffic and sales.\r\n\r\nAll of our practices are â€œWhite Hat Techniquesâ€ and we strictly adhere to Google Webmasters Guidelines to promote our clientsâ€™ websites online.\r\n\r\nIf you are interested, then I can send you our SEO Pricing, Latest Results, and procedure.\r\n\r\nWarm Regards\r\nBethany Smith', '', '2020-09-30 08:49:09'),
(12, 'Kathrine Alexer', 'kathrinealexer@outlook.com', '2096919144', 'hello, connect with us as soon as possible. we need your services for web development and ecommerce for our store and supermart...\r\n\r\n', '', '2020-10-02 02:57:28'),
(13, 'Stevens Billie', 'stevenbill8904@outlook.com', '2523521406', 'Hello, connect with us as soon as possible we need your service for web development and ecommerce for our stores and super marts.', '', '2020-10-12 11:09:23'),
(14, 'Paul Skelcher', 'paul@30secondexplainervideos.com', '3478095956', 'I came across your website after searching for software companies on appfutura.com\r\n\r\nI have a proposition for you... \r\n\r\nBasically what we do is make animated videos that are designed to promote your service online and increase your website conversion rate.\r\n\r\nSo I wanted to offer you an all-inclusive 30 second explainer video for your service for just $197\r\n\r\nAll I ask in return is if you like the video to tell your friends about us! - we are making you this video at cost \r\n\r\nIf you are interested, you can find out more, see examples and get started at www.30secondexplainervideos.com/explainer-promo\r\n\r\nOr you can shoot me a quick email for a brief discussion!\r\n\r\n\r\nCheers,\r\n\r\nPaul\r\nwww.30secondexplainervideos.com/explainer-promo\r\n', '', '2020-10-14 04:40:55'),
(45, 'Testuk', 'Test.uk@gmail.com', '7684512', 'test', '', '2021-02-09 02:11:37'),
(46, 'Mareenamer', 'serioque@vulkan333.com', '83159812385', 'ÐºÐ°Ðº Ð²Ð¾Ð´Ð¸Ñ‚ÑÑ, muzmo Ð±ÐµÑÐ¿Ð»Ð°Ñ‚Ð½Ð¾ ÑˆÐ°Ð½ÑÐ¾Ð½ <a href=https://soundvor.me/mp3/%D1%85%D0%B8%D1%82%D1%8B+2020+%D0%B2+%D0%BC%D0%B0%D1%88%D0%B8%D0%BD%D1%83>ÑÐ°Ð¼Ð°Ñ ÑÐºÐ°Ñ‡Ð¸Ð²Ð°ÐµÐ¼Ð°Ñ Ð¼ÑƒÐ·Ñ‹ÐºÐ° Ð¸Ð½Ñ‚ÐµÑ€Ð½ÐµÑ‚Ð° 2020 Ð² Ð¼Ð°ÑˆÐ¸Ð½Ñƒ</a>', '', '2021-02-09 15:10:36'),
(22, 'ContactGancy', 'no-replyTandanomi@gmail.com', '82437845143', 'HÐµllÐ¾!  sigosoft.com \r\n \r\nDid yÐ¾u knÐ¾w thÐ°t it is pÐ¾ssiblÐµ tÐ¾ sÐµnd mÐµssÐ°gÐµ uttÐµrly lÐµgÐ°l? \r\nWÐµ mÐ°kÐµ Ð°vÐ°ilÐ°blÐµ Ð° nÐµw lÐµgÐ°l mÐµthÐ¾d Ð¾f sÐµnding mÐµssÐ°gÐµ thrÐ¾ugh ÑÐ¾ntÐ°Ñt fÐ¾rms. SuÑh fÐ¾rms Ð°rÐµ lÐ¾ÑÐ°tÐµd Ð¾n mÐ°ny sitÐµs. \r\nWhÐµn suÑh businÐµss Ð¾ffÐµrs Ð°rÐµ sÐµnt, nÐ¾ pÐµrsÐ¾nÐ°l dÐ°tÐ° is usÐµd, Ð°nd mÐµssÐ°gÐµs Ð°rÐµ sÐµnt tÐ¾ fÐ¾rms spÐµÑifiÑÐ°lly dÐµsignÐµd tÐ¾ rÐµÑÐµivÐµ mÐµssÐ°gÐµs Ð°nd Ð°ppÐµÐ°ls. \r\nÐ°lsÐ¾, mÐµssÐ°gÐµs sÐµnt thrÐ¾ugh ÑÐ¾ntÐ°Ñt FÐ¾rms dÐ¾ nÐ¾t gÐµt intÐ¾ spÐ°m bÐµÑÐ°usÐµ suÑh mÐµssÐ°gÐµs Ð°rÐµ ÑÐ¾nsidÐµrÐµd impÐ¾rtÐ°nt. \r\nWÐµ Ð¾ffÐµr yÐ¾u tÐ¾ tÐµst Ð¾ur sÐµrviÑÐµ fÐ¾r frÐµÐµ. WÐµ will sÐµnd up tÐ¾ 50,000 mÐµssÐ°gÐµs fÐ¾r yÐ¾u. \r\nThÐµ ÑÐ¾st Ð¾f sÐµnding Ð¾nÐµ milliÐ¾n mÐµssÐ°gÐµs is 49 USD. \r\n \r\nThis mÐµssÐ°gÐµ is ÑrÐµÐ°tÐµd Ð°utÐ¾mÐ°tiÑÐ°lly. PlÐµÐ°sÐµ usÐµ thÐµ ÑÐ¾ntÐ°Ñt dÐµtÐ°ils bÐµlÐ¾w tÐ¾ ÑÐ¾ntÐ°Ñt us. \r\n \r\nContact us. \r\nTelegram - @FeedbackFormEU \r\nSkype  live:feedbackform2019 \r\nWhatsApp - +375259112693', '', '2020-10-28 20:38:02'),
(25, 'james moslera', 'j.moslera@outlook.com', '4053688421', 'web development project\r\nwe have had a look at your website & believe your expertize will be of great benefit to us. Your services are required in developing a website for Restaurant, Kindly connect with us!!!', '', '2020-11-18 19:54:20'),
(26, 'Mohammed Roshan', 'muhammedroshan.roshan@gmail.com', '8907962086', 'Hi,\r\n\r\nI am in search of an app developer to build a Food Review App.\r\nAppreciate if you could schedule a meeting with your team.', '', '2020-11-30 15:01:43'),
(28, 'Kimberly Van R', 'kimbalithis0@outlook.com', '0000000000', 'I need your help to grow my business.', '', '2020-12-02 02:28:59'),
(30, 'Travis Cole', 'travis@30secondexplainervideos.com', '3478095956', 'I came across your website after searching for marketing agencies on goodfirms.co.\r\n\r\nAnd I was wondering if you would like to partner up?\r\n\r\nBasically, what we do is make animated videos that are designed to promote your service online and increase your websites conversion rate.\r\n\r\nSo, I wanted to offer you a 30 second whiteboard video for your service for just $197 (including script/voiceover)\r\n\r\nAll I ask in return is to tell your friends about us if you like the video \r\n \r\nIf you are interested in this offer, you can find out more and get started at www.30secondexplainervideos.com/whiteboard-sale\r\n	\r\nOr can you shoot me a quick email for a brief discussion!\r\n\r\nCheers,\r\n\r\nTravis\r\nwww.30secondexplainervideos.com/whiteboard-sale\r\n', '', '2020-12-06 17:05:57'),
(31, 'Digital Builders Team', 'business@digitalbuilders.net', '8175176225', 'To whom it may concern, \r\nI am contacting you from Digital Builders.\r\nhttps://www.digitalbuilders.net/\r\nWe are an online directory platform for IT agencies that want to find new customers or partners in the Japanese market. We believe that your company has very interesting services to be listed on our website.\r\nIf you are interested to raise brand awareness and get qualified leads from Japanese companies, then register your account on Digital Builders.\r\nhttps://www.digitalbuilders.net/register/\r\nBest Regards,Digital Builders Team', '', '2020-12-17 00:55:43'),
(32, 'Stores Test.', 'storestestform@gmail.com', '+112312345678', 'Did you know that you can increase the conversion of your app by changing just one element? \r\nJust visit our website and sign up to see example app statistics: https://storestest.com. \r\nAB testing is essentially an experiment where two or more variants of an app are shown to users at random, and statistical analysis is used to determine which variation performs better for conversion. \r\nOur service will create pages with different design variations for your app and collect full statistics about user behavior. \r\nContact us and we will be happy to answer all your questions: help@storestest.com \r\nBest regards, \r\nStoresTest team', '', '2020-12-18 16:15:35'),
(33, 'owen pham', 'owen.pham.c@gmail.com', '441632960081', 'Hi there,\r\n\r\nWould you please be able to put me in touch with the relevant member of your company who is responsible for recruitment? I had a question about one of your open positions.\r\n\r\nKindest,\r\n\r\nOwen\r\n', '', '2021-01-05 17:40:22'),
(34, 'Sophia Lacey', 'vyeddek@khadem.com', 'Tl Crgczzsq', 'Hello,\r\n\r\nDo you want to increase your sales? If you want to increase sales you can simply go to EmailDataPro.com and buy email list. Using this email list you will find new customers and increase your sales. Good luck!\r\n\r\nBest regards,\r\nSophia Lacey', '', '2021-01-20 10:08:31'),
(35, 'Juan Longstreet', 'juan.longstreet@hotmail.com', 'Itqh bvyy', 'Hello\r\n\r\nLetâ€™s face itâ€¦\r\n\r\nSending out emails is fun â€“ as long as you keep getting responsesâ€¦ (But before I get into the details of how you can get more replies, let me tell you a little secret)\r\n\r\nYou might have heard about Abraham Lincolnâ€™s sharpening an axe before chopping a tree quote? If not, here it isâ€¦\r\n\r\nHe said, â€œGive meÂ six hours to chop down a treeÂ and I will spend theÂ first fourÂ sharpening the axe.â€\r\n\r\nNow, how does it apply to you and the way you reach out to people?\r\n\r\nWell, no matter how good you think your email is â€“ no one will open it if its subject line is not enticing enoughâ€¦\r\n\r\nIt wonâ€™t matter if your email brings a lot of value â€“ if the recipient doesnâ€™t open it in the first placeâ€¦\r\n\r\nThis is where the whole â€˜sharpening the axeâ€™ theory steps inâ€¦\r\n\r\nSo, if youâ€™re sending out multiple emails in a day and not getting any responses, thereâ€™s a chance that your emails are not being openedâ€¦\r\n\r\nComing back to how you can get more repliesâ€¦\r\n\r\nOver these years, Iâ€™ve tried and tested multiple subject lines and identified what makes people tickâ€¦\r\n\r\nTo help you out and send the elevator back, Iâ€™ve put all of them together in a done for you, fill in the blank free resource so that you can use them whenever you wantâ€¦\r\n\r\nFollow this link to get access to this free resource and sharpen the axe to get responses like never before!\r\n\r\nhttps://usualprospect.com/emails', '', '2021-01-23 18:08:42'),
(47, 'rajeev@bronetgroup.com', 'rajeev@bronetgroup.com', '9871669723', 'Looking for someone who can develop E-comm site & app for Pharma retail chain. \r\nAppreciate your early response.\r\n\r\nThanks & Regards,\r\n', '', '2021-02-15 10:19:42'),
(48, 'Mr Oliver Wilson', 'oliver@x0ar.org', 'S Y V Xphpf', 'Hi,\r\n\r\nI can see your site had good traffic and is updated frequently, do you offer banner placement or guest posts?\r\n\r\nI am looking to expand the reach of my website and I think a placement here would be benificial.\r\n\r\nTo be clear, I publish sports related content which I think fits with pretty much all niches.\r\n\r\nLet me know what you think.\r\n\r\nBest regards\r\n\r\nOli\r\n\r\noli@x0ar.org', '', '2021-02-18 15:20:59'),
(49, 'Mohit Tiwari', 'sinelogixseo@gmail.com', '09979553686', 'Hello,\r\n\r\nI hope you are doing well.\r\n\r\nI came to know from various resources that you are planning to hire Web Developers. We Sinelogix Technology is a Website Design and Development company based out in Bangalore, India.\r\n\r\nWe have been working in the website business for the last 10 years and supporting various technologies like WordPress, Magento , Laravel, Codigniter, PHP, HTML, Shopify, etc. We are a team of 100+ in-house.\r\n\r\nPlease check our LinkedIn profile: https://www.linkedin.com/company/sinelogix-technology/?originalSubdomain=in\r\n\r\nHere is our cost:\r\n\r\nHere is our cost:\r\n1. PHP developer with more than 5 years of experience: 1000 USD per month\r\n2. WordPress/woo commerce developer with more than 5 years: 800 USD per month\r\n3. Laravel developer with more than 5 years: 1000 USD per month\r\n4. YII Developer with more than 5 years: 1000 USD per month\r\n5. Magento Developer with more than 5 years: 1200 USD per month\r\n\r\nOur hourly rate for WordPress $6/Hr, and for  PHP its $6/Hr\r\n\r\nWith the Developer, we will also assign one Project Manager and tester.\r\nThere is no advance, we can sign monthly contracts and you can pay monthly\r\n\r\nIf you are looking for any help, please email me at mohit.tiwari@sinelogix.com\r\nOr\r\nWhatsApp me at +15105139121 or +91 9979 553 686\r\nMy Skype is: itsdennismiller', '', '2021-02-23 10:46:04'),
(50, 'Pedro  Lucas', 'agent.riquezveronika@email.com', '84943997772', 'Reference  number:  EJ6675. Your winning number: 02 03 16 33 46 + 02. \r\n \r\n(TRANSLATED COPY) \r\n \r\nOFFICIAL WINNING NOTIFICATION \r\nWe are glad to inform you about the release of the EURO JACKPOT lottery draw held on 05-02-2021. Your email address has become a winner in our draw and with the following data: ticket number 3241, reference number: EJ6675. Your winning number: 02 03 16 33 46 + 02. You won in the category - 2Ð„ (5 + 1) and the winning amount is 322.395,30 Euro. \r\n \r\nCongratulations! Your Winning Fund will be ready for payment by order of the lottery company, after clarification and verification of the data. In order to avoid juggling e-mail addresses, we ask you to keep the winning information confidential, to refrain from publicity until your winning data has been processed and the money has been transferred to your account, as this is part of our security protocol to avoid double claims or unreasonable use of this program by participants, as happened in the past. Your email address has been selected automatically, through an automatic computer selection system. All participants were selected through a computer voting system of 100,000 email addresses from a search computer system. \r\n \r\nPlease contact your claims agent/legal office: Agent Veronika  Riquez  to begin your claims call:Tel:+ 34-672-543-214 and Fax:+ 34-632-812-949 Email: segurosmutua001@gmail.com.  Indicate your personal winning number and reference number send to the e-mail: segurosmutua001@gmail.com for your winning application. \r\n \r\nPedro  Lucas, \r\nDirectora de administrativeracion \r\nReply To This Email:  segurosmutua001@gmail.com \r\nEuro jackpot loteria \r\nPhone: + 34-672-543-028', '', '2021-02-24 10:31:02'),
(51, 'Eliza Megan', 'elizamegan66@gmail.com', 'K uck E Svl', 'Hi,\r\n\r\nHow are you doing? I am reaching to offer a simple 3 step process of guest post on your website.\r\n\r\n1. I will send you some interesting topic ideas for a guest post\r\n2. You will choose one topic out of those topic ideas\r\n3. I will then send you a high- quality article on the topic chosen by you\r\n\r\nI would just expect you to give me a do-follow backlink within the main article. So, shall we start with step 1?\r\n\r\nBest,\r\n\r\nEliza Megan', '', '2021-02-25 01:59:29'),
(52, 'Eliza Megan', 'elizamegan66@gmail.com', 'Egq om ox L', 'Hi,\r\n\r\nHow are you doing? I am reaching to offer a simple 3 step process of guest post on your website.\r\n\r\n1. I will send you some interesting topic ideas for a guest post\r\n2. You will choose one topic out of those topic ideas\r\n3. I will then send you a high- quality article on the topic chosen by you\r\n\r\nI would just expect you to give me a do-follow backlink within the main article. So, shall we start with step 1?\r\n\r\nBest,\r\n\r\nEliza Megan', '', '2021-02-25 09:19:56'),
(53, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'This is a test message from sigosoft.com  for testing from is working or not.', '', '2021-02-27 04:16:53'),
(54, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'Test message from sigosoft.com', 'sigosoft.com', '2021-02-27 04:48:46'),
(55, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'Test message from sigosoft.ae', 'sigosoft.ae', '2021-02-27 04:51:46'),
(56, 'Shana', 'shana.sigosoft@gmail.com', '9745807214', 'This is a test message from sigosoft.co.uk', 'sigosoft.co.uk', '2021-02-27 04:54:36'),
(57, 'DonaldQuary', 'no-replyBeltIrrette@gmail.com', '82257648379', 'Hello!  sigosoft.ae \r\n \r\nDid you know that it is possible to send appeal totally legal? \r\nWe put a new method of sending letter through contact forms. Such forms are located on many sites. \r\nWhen such commercial offers are sent, no personal data is used, and messages are sent to forms specifically designed to receive messages and appeals. \r\nalso, messages sent through feedback Forms do not get into spam because such messages are considered important. \r\nWe offer you to test our service for free. We will send up to 50,000 messages for you. \r\nThe cost of sending one million messages is 49 USD. \r\n \r\nThis offer is created automatically. Please use the contact details below to contact us. \r\n \r\nContact us. \r\nTelegram - @FeedbackMessages \r\nSkype  live:contactform_18 \r\nWhatsApp - +375259112693', 'sigosoft.ae', '2021-03-07 23:46:10'),
(58, 'Joe Miller', 'info@domainregistrationcorp.com', '+1542384593234', 'Notice#: 491343\r\nDate: 13 Mar 2021\r\n\r\nYOUR IMMEDIATE ATTENTION TO THIS MESSAGE IS ABSOLUTELY NECESSARY!\r\n\r\nYOUR DOMAIN sigosoft.in WILL BE TERMINATED WITHIN 24 HOURS\r\n\r\nWe have not received your payment for the renewal of your domain sigosoft.in\r\n\r\nWe have made several attempts to reach you by phone, to inform you regarding the TERMINATION of your domain sigosoft.in\r\n\r\nCLICK HERE FOR SECURE ONLINE PAYMENT: https://registerdomains.ga\r\n\r\nIF WE DO NOT RECEIVE YOUR PAYMENT WITHIN 24 HOURS, YOUR DOMAIN sigosoft.in WILL BE TERMINATED\r\n\r\nCLICK HERE FOR SECURE ONLINE PAYMENT: https://registerdomains.ga\r\n\r\nACT IMMEDIATELY.\r\n\r\nThe submission notification sigosoft.in will EXPIRE WITHIN 24 HOURS after reception of this email.', 'sigosoft.co.uk', '2021-03-13 17:50:05'),
(59, 'Luis Juan', 'luisjuan0911@outlook.com', '9895021543', 'Hello i just established a new company and i need a company to design and develop a website for me ', 'sigosoft.com', '2021-03-20 12:02:08'),
(60, 'Kimberly Reeves', 'ogri.goshen10@outlook.com', '0000000000', 'I need your help to grow my business.', 'sigosoft.com', '2021-03-23 00:01:57'),
(61, 'jo', 'jo@gmail.com', '9685744152', 'testing only', 'sigosoft.com', '2021-04-01 03:29:00'),
(62, 'Jo', 'jo@gmail.com', '9713466778', 'Testing ', 'sigosoft.com', '2021-04-01 03:29:47'),
(63, 'Jo', 'jo@gmail.com', '9857452379', 'Test', 'sigosoft.com', '2021-04-01 03:30:19'),
(64, 'test', 'test2@gmail.com', '9876543210', 'test3', 'sigosoft.com', '2021-04-19 09:31:52');

-- --------------------------------------------------------

--
-- Table structure for table `freelancer`
--

CREATE TABLE `freelancer` (
  `id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `numberof` varchar(100) NOT NULL,
  `emailid` varchar(100) NOT NULL,
  `areaof` varchar(100) NOT NULL,
  `about` varchar(500) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `freelancer`
--

INSERT INTO `freelancer` (`id`, `fullname`, `numberof`, `emailid`, `areaof`, `about`, `timestamp`) VALUES
(1, 'Nabhan k', '9746613993', 'nabhan.pkl@gmail.com', 'ios', 'I enjoy being challenged and working on projects that require me to work outside my comfort and knowledge set, as continuing to learn new languages and development techniques are important to me and the success of your organization.', '2018-03-05 04:09:34'),
(19, 'JAZEEL K T', '8075752837', 'jazeelkt@gmail.com', 'ios', 'iOS expert with over 6 years of experience. Worked on all stages of app development, from concept up to AppStore release. Expertise in iOS frameworks like Core Graphics, Core Animation etc. Have also worked with CI/CD pipelines for iOS using Fastlane. Experience in both Objective C and Swift.', '2018-03-13 05:28:44'),
(18, 'Muhammad Mubarak K', '9567474709', 'mubarak@inkers.in', 'Android', 'Freelance Android developer', '2018-03-11 15:50:43'),
(17, 'Aswanth R Chandran', '9495575224', 'aswanthrc@gmail.com', 'Android', 'An Android developer with 4+ years of experience', '2018-03-11 12:31:18'),
(16, 'Arjun C L', '9567129796', 'arjunclarjun@gmail.com', 'Android', 'Native Android Developer with 2 years of experience', '2018-03-11 12:21:56'),
(15, 'shamon', '7736527089', 'shamonsha665@gmail.com', 'web_development', '3.5 years experience in  web development.\r\nLanguages:Php, JavaScript.\r\nFrameworks:Laravel,codeigniter expressjs.\r\nDatabases:MySQL,Mongodb.\r\n\r\n\r\n\r\n', '2018-03-11 11:55:54'),
(20, 'Muhammed Ali', '9995935572', 'mhdali.myk@gmail.com', 'Android', 'I am a android developer', '2018-04-25 13:58:10'),
(21, 'Ganesh', '8098500319', 'Ganezz.ganesh@gmail.com', 'Android', 'I do have 1 year of experience as a Android Developer. I am willing to support you on your projects. Kindly consider. ', '2018-05-07 09:49:19'),
(22, 'Arathy MS', '9446781094 ', 'ms.arathynair@gmail.com ', 'crossplatform', 'An experienced QA professional with all the levels of manual testing. Specifically  functional, UAT, performance testing. ', '2018-05-17 08:30:51'),
(23, 'SANIYA THAHSEEN M', '9562494769', 'ayinasmadala@gmail.com', 'Android', 'Now, completed BSC computer science.developed Android application my self.', '2018-05-28 08:48:14'),
(24, 'AYSHA YAMSHIDA K', '9497244305', 'ayshayamshida927@gmail.com', 'Android', 'I am hard working, sincere and dedicated women confident enough to reach the unreached.', '2018-06-09 08:29:10'),
(25, 'Sankerdas.s', '8089953856', 'sankerdas.yes@gmail.com', 'Web_designing', 'I am a freelancer full stack web developer with 2 years of experience.\r\nI am ready to do any web related works.', '2018-06-19 05:47:56'),
(26, 'Fahad K P ', '7598569739', 'fahadkp63@gmail.com', 'web_development', 'I completed my B-tech IT graduation from Christian college of engineering and technology affiliated to Anna University.  After college I have completed a 4 month training course on php, html, css, sql, javascript. And I have worked as Php developor in a US based company whose back office is at our native. ', '2018-06-23 02:09:51'),
(27, 'qkluhfgl', '1', 'sample@email.tst', 'Web_designing', '1', '2018-06-24 09:45:28'),
(28, 'pjxipnxu', '1', 'sample@email.tst', 'web_development', '1', '2018-06-24 09:45:28'),
(29, 'srrytwsc', '1', 'sample@email.tst', 'DM/SEO', '1', '2018-06-24 09:45:28'),
(30, 'kxttvlje', '1', 'sample@email.tst', 'Android', '1', '2018-06-24 09:45:28'),
(31, 'grxsrbvf', '1', 'sample@email.tst', 'ios', '1', '2018-06-24 09:45:29'),
(32, 'vvskwqqf', '1', 'sample@email.tst', 'crossplatform', '1', '2018-06-24 09:45:29'),
(33, 'Shaheer mon m', '8848982672', 'shaheermalayil@outlook.com', 'Web_designing', 'I am Shaheer completed MCA . My key programming skilss include c c++,java \r\nAnd I have proficiency in php , Python Flak,and bootstrap,javascript. i know basics of android development as i am done a my degree  project in android', '2018-07-13 03:03:52'),
(34, 'vijesh', '9633580721', 'vijesh721@gmail.com', 'Android', 'please contract', '2018-07-22 16:14:05'),
(35, 'Chandramohan', '7025678946', 'kpcmohan1991@gmail.com', 'ios', 'Have 2.6 years experience in iOS app development\r\n\r\nFluent in both Swift and Objective C', '2018-08-03 15:47:39'),
(36, 'Mohammed Ameen', '9946642104', 'thotathilameen@gmail.com', 'web_development', 'I am a PHP Developer having more than 1 year experience in developing and maintaining core PHP web applications / websites along with developing customized Web designs using technologies like HTML5, CSS3, Javascript, Jquery etc. I am seeking for a challenging job opportunity that would synergize my skills and knowledge that cooperates with the objectives of the organization.', '2019-01-22 10:22:28'),
(37, 'ANURAG K P', '9526320505', 'anuragkp08@gmail.com', 'Android', 'Respected Sir,\r\n                   \r\n             Am Anurag from kannur,Am completing my btceh in computer science.Last two year am working in a private sector  as  android developer.Am working from home, So am interested to  working as a freelancer in your company.\r\n\r\n   Thank you', '2019-01-23 05:52:30'),
(38, 'ANURAG K P', '9526320505', 'anuragkp08@gmail.com', 'Android', 'Respected Sir,\r\n     Am Anurag from kannur,Am completed  Btech in Computer Science.Last two year am working as an android developer in  a private sector.Am working from home,So am interested to working as a freelancer in your company.\r\nAm  hopefully waiting for your responds.\r\nThanks.', '2019-01-23 06:57:24'),
(39, 'kabil', '9745317909', 'k.kabil17@gmail.com', 'web_development', 'Dear Recruiting Team,,\r\n\r\n       I am writing this letter to apply for the \"IT support / Document Controller / eCommerce co-ordinate / Web Design & Developmentâ€ position. I have enclosed my resume. If you have any vacancy, please let me know. I look forward to discussing my qualifications further and can be reached by email at k.kabil17@gmail.com or by phone at (+91) 9745317909.\r\n\r\nWORKING EXPERIENCE\r\n\r\n\r\n      - Overall 2+ years of IT experience in Web Application Development.\r\n\r\n      - Soun', '2019-03-26 09:23:35'),
(40, 'sanoop', '9605481138', 'sanoopgopinath9@gmail.com', 'crossplatform', 'Dear Sir/Madam\r\n\r\nI am having 4+ years of software test engineer experience (Manual and Automation) In BANKING DOMAIN at SESAME SOFTWARE SOLUTION calicut.\r\n\r\nI have experience in Automating Web Application Testing using Selenium WebDriver with TestNG framework.\r\nWell versed with Handling Elements in Selenium WebDriver and Writing Test cases using Element locators, WebDriver methods, Java programming features and TestNG Annotations.\r\n\r\nAs a QA person, I have written Test Script, Test Cases throug', '2019-04-04 09:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `uid` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`uid`, `username`, `password`) VALUES
(1, 'info@sigosoft.com', '88d25e7acc2062702ba73f6ef2cfbc6f');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `post` varchar(100) NOT NULL,
  `linkedin` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `image` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`id`, `name`, `post`, `linkedin`, `facebook`, `image`) VALUES
(16, 'Aithin EP', 'PHP Developer', 'http://linkedin.com/in/aithin-ep/', 'https://www.facebook.com/aithin.calicut', 'uploads/248573.jpeg'),
(17, 'Pooja Manoj', 'Android Developer', 'https://www.linkedin.com/in/pooja-manoj/', '', 'uploads/898360.jpg'),
(18, 'Shyamily PV', 'PHP Developer', 'https://www.linkedin.com/in/shyamily-pv-74938773/', 'https://www.facebook.com/shyamily.pv', 'uploads/560463.jpg'),
(19, 'Swathi PK', 'Junior Web Developer', 'https://www.linkedin.com/in/swathi-p-k-209299169/', 'https://www.facebook.com/swathi.satheesh.14', 'uploads/918573.jpg'),
(21, 'Fathima Shifna K', 'Android Developer', 'https://www.linkedin.com/in/fathima-shifna-386074b4/', '', 'uploads/620386.jpg'),
(22, 'Shana K', 'PHP Developer', 'https://www.linkedin.com/in/shana-k-a63582166', '', 'uploads/208889.jpg'),
(23, 'Jeeshna TK', 'Android Developer', 'https://www.linkedin.com/in/jeeshna-t-k-711831166', 'https://www.facebook.com/profile.php?id=100009065605962', 'uploads/52243.jpg'),
(42, 'Abhijith K P', 'Android Developer', '', '', 'uploads/385720.jpg'),
(41, 'Shahna Hassan', 'IOS Developer', 'https://www.linkedin.com/in/shahna-hassan-b34618160/', 'https://www.facebook.com/shahna.hassan.7', 'uploads/380810.jpg'),
(40, 'Chaithannya K', 'BDE', '', 'https://www.facebook.com/chaithanyavedamani', 'uploads/318348.jpg'),
(48, 'Anagha ', 'Android Developer', '', '', 'uploads/290407.jpg'),
(49, 'Anas K.P', 'Magento Developer', '', '', 'uploads/374698.jpg'),
(50, 'Lihindas', 'PHP Developer', 'https://www.linkedin.com/in/lihindas-ph-62b1b8b4/', 'https://www.facebook.com/lihin.ph', 'uploads/843504.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`CareerID`);

--
-- Indexes for table `careers`
--
ALTER TABLE `careers`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `consultation`
--
ALTER TABLE `consultation`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`ContactID`);

--
-- Indexes for table `freelancer`
--
ALTER TABLE `freelancer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `career`
--
ALTER TABLE `career`
  MODIFY `CareerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `careers`
--
ALTER TABLE `careers`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `consultation`
--
ALTER TABLE `consultation`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `ContactID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `freelancer`
--
ALTER TABLE `freelancer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
