<!-- preloader begin -->
<div class="preloader">
<img src="assets/img/logo-sigosoft.png" alt="Sigosoft Logo" />
</div>
<!-- preloader end -->
<style>
   @media only screen and (min-width: 320px) and (max-width: 575px){
   .header-2 .bottombar .mainmenu .navbar .navbar-nav .nav-item.dropdown:hover .dropdown-menu {
    /* display: block; 
   padding: 0; */
   margin: 0 15px;
   border: none;
   }
   }
</style>
<!-- header begin -->
<div class="header-2">
   <div class="topbar">
      <div class="container this-container">
         <div class="row">
            <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
               <p class="welcome-text">We provide worldwide services 24/7</p>
            </div>
            <div class="col-xl-6 col-lg-6">
               <div class="support-bars">
                  <ul>
                     <li><a href="mailto:info@sigosoft.com">
                        <span class="icon"><i class="fas fa-envelope-open"></i></span>
                        info@sigosoft.com</a>
                     </li>
                     <li><a href="tel:+919846237384">
                        <span class="icon"><i class="fas fa-phone"></i></span>
                        +91 9846237384</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="bottombar">
      <div class="container this-container">
         <div class="row">
            <div class="col-xl-12 col-lg-12">
               <div class="mainmenu">
                  <nav class="navbar navbar-expand-lg">
                    <a class="navbar-brand" href="#"><img src="assets/img/logo-sigosoft.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <i class="fas fa-bars"></i>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                           <li class="nav-item">
                              <a class="nav-link" href=".">Home</a>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              About
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                 <a class="dropdown-item" href="about">Our Profile</a>
                                 <a class="dropdown-item" href="team">Our Team</a>                                                   
                                 <a class="dropdown-item" href="technologies">Our Technologies</a>
                                 <a class="dropdown-item" href="partner-with-us">Partner with Us</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Services
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                 <a class="dropdown-item" href="mobile-app-development-company-in-calicut-india">Mobile App Consultation</a>
                                 <a class="dropdown-item" href="android-app-development">Native Android App Development</a>
                                 <a class="dropdown-item" href="ios-app-development">Native iOS App Development</a>
                                 <a class="dropdown-item" href="flutter-app-development">Flutter App Development</a>
                                 <a class="dropdown-item" href="react-native-development">React Native Development</a>
                                 <a class="dropdown-item" href="mobile-app-ui-designing">Mobile App UI Designing</a>
                                 <a class="dropdown-item" href="mobile-app-testing">Mobile App Testing</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Products
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                 <div class="dmw500">
                                       <div class="navsplit">
                                          <a class="dropdown-item" href="ecommerce-mobile-app-development">E-Commerce Apps</a>
                                          <a class="dropdown-item" href="loyalty-mobile-app-development">Loyalty Apps</a>
                                          <a class="dropdown-item" href="e-learning-mobile-app-development">E-Learning Apps</a>
                                          <a class="dropdown-item" href="community-mobile-app-development">Community Apps</a>
                                          <a class="dropdown-item" href="supply-chain-mobile-app-development">Supply Chain Apps</a>
                                          <a class="dropdown-item" href="rent-a-car-mobile-app">Rent a Car Apps</a>
                                          <a class="dropdown-item" href="telemedicine-app-development">TeleMedicine  Apps</a>
                                       </div>
                                       <div class="navsplit">
                                          <a class="dropdown-item" href="food-delivery-app-development">Food Delivery Apps</a>
                                          <a class="dropdown-item" href="classified-app-development-classified-app-builder">Classified Apps</a>
                                          <a class="dropdown-item" href="air-ticket-booking-app-for-android-iOS">Flight Booking Apps</a>
                                          <a class="dropdown-item" href="hotel-booking-app-development">Hotel Booking Apps</a>
                                          <a class="dropdown-item" href="sports-app-development-company">Sports Apps</a>
                                          <a class="dropdown-item" href="car-wash-app-development">Car Wash Apps</a>
                                          <a class="dropdown-item" href="van-sales-mobile-application">Van sales Apps</a>
                                       </div>
                                    </div>
                              </div>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link port" href="portfolio">Portfolio</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" href="blog">Blogs</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" href="careers">Careers</a>
                           </li>
                           <li class="nav-item">
                              <a class="nav-link" href="contact">Contact</a>
                           </li>
                        </ul>
                        <div class="header-buttons">
                           <a href="contact.php" class="quote-button">Get A Quote</a>
                        </div>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </div>
      <style>
         .header-2 .bottombar .header-buttons {
         width: auto !important;
         }
         .dmw500{
            width:450px;
            display:flex !important;
            flex-direction: row;
         }
         .dmw500 .navsplit{
            width:50%;
         }
         .port{
         background: -webkit-linear-gradient(21deg,#d63438 0,#90191c 100%) !important;
         color: #fff !important;
         height: 50px;
         margin-top: 20px !important;
         padding: 14px !important;
         border-radius: 3px;
         }
         .header-2 .bottombar .mainmenu .navbar .navbar-nav .nav-item .nav-link.port:before{
         background: #0000;
         }
         @media only screen and (max-width: 991px) {
         .port{
         margin-top: auto !important;
         }
         .dmw500{
            width:100%;
            flex-direction: column;
         }
         .dmw500 .navsplit{
            width:100%;
         }
         }
      </style>
   </div>
</div>
<!-- header end -->