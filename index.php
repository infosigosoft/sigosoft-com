<!doctype html>
<html lang=en>
<head>
<title>Sigosoft, Mobile App Development Company in India, USA</title>
<meta charset=utf-8>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Mobile App Development Company"/>
<meta property="og:description" content="We are the leading Mobile App Development company. We help to provide  robust and reliable Mobile App Development service."/>
<meta property="og:url" content="https://www.sigosoft.com"/>
<meta property="og:site_name" content="Sigosoft"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="We are the leading Mobile App Development company. We help to provide  robust and reliable Mobile App Development service! "/>
<meta name="twitter:title" content="Mobile App Development Company" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>

<meta content="We are the leading Mobile App Development company. We help to provide robust and reliable Mobile App Development service in India and USA." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
    <?php include('styles.php'); ?>
</head>
    <body>

        

        <?php include('header.php'); ?>

        <!-- banner begin -->
        <div class="banner-2">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-center justify-content-center">
                    <div class="col-xl-5 col-lg-10 col-md-8">
                        <div class="banner-content">
                            
                            <h1> Top-Notch<span class="special"> Mobile App<br></span> Company</h1>
                            <p>Quality Mobile App Development services focussed to match your business goals.</p>
                            <a class="btn-murtes banner-button" href="portfolio">Explore More <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="part-img">
                            <div class="shape-1">
                                <img src="assets/img/shape-1.png" alt="">
                            </div>
                            <div class="shape-2">
                                <img src="assets/img/shape-2.png" alt="">
                            </div>
                            <img class="readius" src="assets/img/bg-banner-home2.jpg" alt="">
                            <div class="play-button">
                                <a class="mfp-iframe" href="https://www.youtube.com/watch?v=w-aNw1zdyrs"><i class="fas fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->

        <!-- statics begin -->
        <!--<div class="statics statics-2">-->
        <!--    <div class="container">-->
        <!--        <div class="row">-->
        <!--            <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="500">-->
        <!--                <div class="single-statics">-->
        <!--                    <span class="number">6+</span>-->
        <!--                    <span class="title">Years of experience</span>-->
        <!--                    <div class="bg-icon">-->
        <!--                        <img src="assets/img/svg/timetable.svg" alt="">-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="1000">-->
        <!--                <div class="single-statics">-->
        <!--                    <span class="number">100+</span>-->
        <!--                    <span class="title">Total projects</span>-->
        <!--                    <div class="bg-icon">-->
        <!--                        <img src="assets/img/svg/contract.svg" alt="">-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="1500">-->
        <!--                <div class="single-statics">-->
        <!--                    <span class="number">10+</span>-->
        <!--                    <span class="title">Products</span>-->
        <!--                    <div class="bg-icon">-->
        <!--                        <img src="assets/img/svg/mobile-home.png" alt="">-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--            <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="2000">-->
        <!--                <div class="single-statics">-->
        <!--                    <span class="number">100+</span>-->
        <!--                    <span class="title">Happy clients</span>-->
        <!--                    <div class="bg-icon">-->
        <!--                        <img src="assets/img/svg/happiness.svg" alt="">-->
        <!--                    </div>-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <!--    </div>-->
        <!--</div>-->
        <!-- statics end -->

      <!-- overview begin -->
        <div class="overview">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-5 col-lg-5 col-md-6">
                        <div class="part-img d-none d-sm-block">
                            <div class="shape-1">
                                <img src="assets/img/shape-1.png" alt="">
                            </div>
                            <div class="shape-2">
                                <img src="assets/img/shape-3.png" alt="">
                            </div>
                            <img class="radius" src="assets/img/home-about.png" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="part-text">

                    <h3>Best Mobile App Company</h3>

                            <p>Sigosoft is one of the topmost Mobile App development companies in India. Our team consists of experienced analysts, designers, and developers who leverage cutting-edge development technology to develop quality and productive mobile applications. We have a legacy of successfully delivering custom Mobile Apps in android, iOS and cross-platform technologies like flutter and React Native.</p>
<p>Sigosoft is one of most the most touted Mobile App Development companies, and we are located in Calicut, the cultural hub of Kerala. Our skilled team is always abreast of the latest advanced technologies & tools to build Mobile Apps for your business model. Join hands with us and we assure you that you will never leave us disappointed. We give the utmost value to your business goals and strive hard to offer the best service to you.</p>
                         <a href="about.php" class="btn-murtes">Read More + <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview end -->
        <!-- choosing reason begin -->
        <div class="choosing-reason-2">
            <div class="container this-container">
                <div class="row justify-content-center">
                    <div class="col-xl-9 col-lg-9 col-md-8">
                        <div class="section-title-2 text-center">
                            
                            <!--<h3>Why <span class="special">Choose</span> Us?</h3>-->
           <h2 style="padding-top:5px;"> What Makes <span class="special">Sigosoft</span> the best?</h2>
                            <p>Wondering what makes Sigosoft unique and approachable? Our team members are committed to delivering the most effective, unique, and appealing Mobile App services to you. Also check out our features and find out the reason behind our long list of loyal customers. </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="new-animation" data-aos-delay="250" data-aos-duration="500">
                        <div class="single-reason">
                            <div class="part-head">
                                <span class="number">01</span>
                            </div>
                            <div class="part-body">
                                <h3>Talented and Experienced Team</h3>
                                <p>Our team comprises of highly talented and experienced app developers. We have great expertise in offering apt and effective solutions to customers in order to attain their business goals.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="new-animation" data-aos-delay="250" data-aos-duration="1000">
                        <div class="single-reason">
                            <div class="part-head">
                                <span class="number">02</span>
                            </div>
                            <div class="part-body">
                                <h3>Quality Services</h3>
                                <p>Want to enjoy top quality services at affordable rates? If yes, Sigosoft is the right choice for you. We consider quality service as the key factor to successful, loyal, and long-term customer relationships.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="new-animation" data-aos-delay="250" data-aos-duration="1500">
                        <div class="single-reason">
                            <div class="part-head">
                                <span class="number">03</span>
                            </div>
                            <div class="part-body">
                                <h3>Great Customer Support</h3>
                                <p>We offer the best in class quality support to our customers through our Ticketing system. With us, you can ensure effective customer support 24/7. </p>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
        <!-- choosing reason end -->

        <!-- overview begin -->
        <div class="overview">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-5 col-lg-5 col-md-6">
                        <div class="part-img">
                            <div class="shape-1">
                                <img src="assets/img/shape-1.png" alt="">
                            </div>
                            <div class="shape-2">
                                <img src="assets/img/shape-3.png" alt="">
                            </div>
                            <img class="radius" src="assets/img/bg-overview.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="part-text">

                    <h3>Mobile App Development Services</h3>

                            <p>We provide quality end to end Mobile App Development assistance. Our Mobile consulting team is adept at providing the right technical guidance to help you set up the perfect system design for your requirement.  
 
With more than  6 years of experience, we deliver secure, scalable, and high-end mobile & web applications. Our efficient development procedures streamlined Mobile Apps are focussed to build various apps for a various platforms such as android, iOS, flutter, and react native. </p>

                            <a href="about.php" class="btn-murtes">Read More + <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        <div class="part-video">
                            <img src="assets/img/bg-service.jpg" alt="">
                            <!--<a class="play-button mfp-iframe" href="https://www.youtube.com/watch?v=NU9Qyic_mX0">
                                <i class="fas fa-play"></i>
                            </a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- overview end -->




        <!-- service begin -->
        <div class="service-2">
            <div class="container">
                <div class="this-section-title">
                    <div class="row justify-content-between">
                        <div class="col-xl-6 col-lg-6">
                            <h2>Why we are the Best Mobile App Development Company?</h2>
                        </div>
                        <div class="col-xl-5 col-lg-5 d-xl-flex d-lg-flex d-block align-items-center">
                            <p>There are many Mobile App development companies. But want to know what's the secret behind our growing success?</p>
                        </div>
                    </div>
                </div>
                
                <div class="service-2-slider owl-carousel owl-theme">

                    <div class="single-servcie">
                        <h3 class="service-title">24/7 Customer<br/> Support                            
                            <span class="bg-number">01</span>
                        </h3>
                        <p class="service-content">Who would develop an app for my business? Will it be convenient for all the users who use it? So many questions! One answer, Sigosoft! Our customer support is here to guide you and provide solutions to all your queries.</p>
                        <!-- <a href="#" class="service-details-button">Details <i class="fas fa-long-arrow-alt-right"></i></a>-->
                    </div>

                    <div class="single-servcie">
                        <h3 class="service-title">Proactive<br/> Efficient Team
                                <span class="bg-number">02</span>
                        </h3>
                        <p class="service-content">Want some great ideas for your mobile app? Our team members are always encouraged to provide out-of-the-box solutions and that is a major factor that helps us remain as a major custom mobile app development company.</p>
                         <!--<a href="#" class="service-details-button">Details <i class="fas fa-long-arrow-alt-right"></i></a>-->
                    </div>

                    <div class="single-servcie">
                        <h3 class="service-title">Best quality mobile apps and services
                                <span class="bg-number">03</span>
                        </h3>
                        <p class="service-content">Quality is the success driving factor for Sigosoft! No clients are left unsatisfied with our innovative results because they keep coming for more!</p>
                       <!-- <a href="#" class="service-details-button">Details <i class="fas fa-long-arrow-alt-right"></i></a>-->
                    </div>

                    <div class="single-servcie">
                        <h3 class="service-title">Mobile app development exposure and experience
                                <span class="bg-number">04</span>
                        </h3>
                        <p class="service-content">Can you guess how many projects we have handled in our 5+ years of experience? 100+! These projects have helped us gain infinite knowledge and hence become a trustworthy expert in the mobile app development services.</p>
                        <!-- <a href="#" class="service-details-button">Details <i class="fas fa-long-arrow-alt-right"></i></a>-->
                    </div>

                    
                </div>
            </div>
        </div>
        

        <!-- service begin -->
        <div class="service service-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-10 col-lg-10 col-md-10">
                        <div class="section-title-2 text-center">
                            <h2 class="subtitle">Our Services</h2>
                            <p>We provide the best Mobile Application Development Service in India. We work for providing the qualified Mobile applications to our clients. Regardless of whether it's making versatile arrangements we provide you the best application for your business or a portable advanced mission - each arrangement requires a ton of preparation and system to go into the engineering of the end-user experience and how can it incorporate once again into your current business.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/android-app-development.jpg" alt="Android App Development Company Calicut">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Native Android App Development
                                    <span class="bg-number">01</span>
                                </h3>
                                <p class="service-content">With us, you can get 100% quality android app development services. Our expert team of android app developers create unique strategies to develop a mobile application for the clients to achieve their business goals.</p>
                                <a href="https://www.sigosoft.com/android-app-development.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/ios-app-development.jpg" alt="iOS App Development Company in Calicut, India">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Native iOS App Development
                                    <span class="bg-number">02</span>
                                </h3>
                                <p class="service-content">When it comes to mobile application, iOS apps play a significant role. Now, want to have an iOS app for your business? If yes, then reach us. We are the leading iOS app development company in India.</p>
                                <a href="https://www.sigosoft.com/ios-app-development.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/flutter-development.png" alt="Flutter App Development Company ">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Flutter App Development
                                    <span class="bg-number">03</span>
                                </h3>
                                <p class="service-content">Being probably the best proponents of iPhone, Android, and Windows applications in India, we are offering first rate class application development services for Flutter apps also. Reach us for the qualified Flutter app development for your business.</p>
                                <a href="https://www.sigosoft.com/flutter-app-development.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/React-Native-Development.png" alt="React Native Development">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">React Native Development
                                    <span class="bg-number">04</span>
                                </h3>
                                <p class="service-content">With react native framework, we have constructed many cross-platform for new businesses to industry monsters. We have a reputed name among the top React Native application improvement organizations with 100% fulfilled customers on each edge of the world.</p>
                                <a href="https://www.sigosoft.com/react-native-development.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/Mobile-App-UI-Designing.png" alt="Mobile App UI Designing">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Mobile App UI Designing
                                    <span class="bg-number">05</span>
                                </h3>
                                <p class="service-content">As an inventive UI/UX plan company, we cut a client first interface and portable mobile app development experience, with a center spotlight on your business objectives. Our creators make the application plan that inhales your idea, by developing with the most recent industry patterns.</p>
                                <a href="https://www.sigosoft.com/mobile-app-ui-designing.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/Mobile-App-Testing.png" alt="Mobile App Testing">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Mobile App Testing
                                    <span class="bg-number">06</span>
                                </h3>
                                <p class="service-content">We give end-to-end testing to mobile applications, including practical, security, execution, similarity, convenience, and test computerization. We are a renowned mobile application testing organization that offers astounding versatile application testing administration.</p>
                                <a href="https://www.sigosoft.com/mobile-app-testing.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>


                    <!--<div class="col-xl-4 col-lg-4 col-md-6 offset-lg-2" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">-->
                    <!--    <div class="single-servcie">-->
                    <!--        <div class="part-img">-->
                    <!--            <img src="assets/img/services/ecommerce-development.jpg" alt="E-commerce Development">-->
                    <!--        </div>-->
                    <!--        <div class="part-text">-->
                    <!--            <h3 class="service-title">E-commerce Development-->
                    <!--                <span class="bg-number">04</span>-->
                    <!--            </h3>-->
                    <!--            <p class="service-content">Facing difficulty in finding the best eCommerce website development service provider? Don’t struggle more for this. We are here for you. Being the best mobile app development company, here you can ensure the best and quality service.</p>-->
                    <!--            <a href="https://www.sigosoft.com/ecommerce-website-development.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->

                    <!--<div class="col-xl-4 col-lg-4 col-md-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">-->
                    <!--    <div class="single-servcie">-->
                    <!--        <div class="part-img">-->
                    <!--            <img src="assets/img/services/magento-development.jpg" alt="Magento Development">-->
                    <!--        </div>-->
                    <!--        <div class="part-text">-->
                    <!--            <h3 class="service-title">Magento Development-->
                    <!--                <span class="bg-number">05</span>-->
                    <!--            </h3>-->
                    <!--            <p class="service-content">Want a user-friendly, reliable, and secured eCommerce site using Magento platform? We can help you with the best Magento development services at affordable rates.</p>-->
                    <!--            <a href="magento-web-development-company.php" class="service-details-button">Discover Now <i class="fas fa-long-arrow-alt-right"></i></a>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->

                

                </div>
            </div>
        </div>
        
        <!-- overview begin -->
        <div class="overview home-overview-2 pb-0">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-5 col-lg-5 col-md-6">
                        <div class="part-img">
                            <div class="shape-1">
                                <img src="assets/img/shape-1.png" alt="">
                            </div>
                            <div class="shape-2">
                                <img src="assets/img/shape-3.png" alt="">
                            </div>
                            <img class="radius" src="assets/img/bg-overview4.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="part-text">

                            <h2 class="subtitle">Top Mobile App Development services</h2>
                            <p>We at Sigosoft believe in making our work speak for our quality services. 
                            With us, you sign up for a great team that knows the Knitty gritty of the Mobile App Development processes that will suit your business needs. 
                            Our team has a cumulative experience of more than 6 years, which we use to guide you with developing the best Mobile App for your business. 
Needless to say, these make us one of the best Mobile App Development companies in Kerala, India. 
We are growing across the globe to remain the #No.1 Mobile App Development company not only in India but overseas! 
What keeps inspiring us to keep innovating new Mobile App is our beloved clients.
.</p>


                            <a href="about.php" class="btn-murtes">Read More + <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- overview end -->



        <!-- about begin -->
        <div class="about">
            <div class="container about-container">
                <div class="row justify-content-center">
                    <div class="col-xl-9 col-lg-9 col-md-9">
                        <div class="section-title text-center">
                             <h2 class="subtitle">Want the best Mobile App company<br> to work with you on your next project?</h2>
                            <p>Join your hands with Us to enjoy premium Mobile App Development services.</p>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-xl-between justify-content-lg-center justify-content-between">
                    <div class="col-xl-4 col-lg-5 col-md-6">
                        <div class="part-text left">
                            <h3>Want any advise on how to develop robust and scalable Mobile Applications?</h3>
                            <p>Work with the best Mobile App developers who provide full-cycle Mobile App Development solutions to scale up your business. 
                            Our agile Mobile App developers have in-depth knowledge about the latest technologies that suit your business the best.</p>
                            <p>We understand your requirements and curate a custom made Mobile App that is just perfect to elevate your business model.</p>
                         
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 d-xl-block d-lg-none d-md-none d-block">
                        <div class="part-img">
                            <div class="shape-one">
                                <img src="assets/img/shape-1.png" alt="">
                            </div>
                          
                            <img src="assets/img/bg-overview2.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="part-text right">
                            <h3>Sigosoft constructs better paths to reach great heights in your business</h3>
</h3>
                            <p>Our web and full-stack capabilities include java, react JS, angular JS , php, Swift, etc. We use cutting edge technologies to deliver the best mobile apps, web apps, and other software services. </p>


<p> Our customer support team is well trained to provide amicable and useful services at the time of your needs. 
</p>
                            <a href="about.php" class="learn-more-button">Learn More <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- about end -->
        

        <!-- testimonial begin -->
        <div class="testimonial">
            <div class="container">
                <div class="this-section-title">
                    <div class="row justify-content-center">
                        <div class="col-xl-6 col-lg-7 col-md-8">
                            <div class="section-title-1 text-center">
                                <h2 class="subtitle">What our customers say about us</h2>
                                <p>Your feedback is our backbone</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testimonial-slider owl-carousel owl-theme">
                    <div class="single-testimonial">
                        <div class="part-body">
                            <div class="rate">
                                <ul>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p>"I am really thankful to the entire team of Sigosoft for their appreciable effort and their remarkable dedication to have our website live. Sigosoft made it awesome and meeted our expectations."</p>
                        </div>
                        <div class="part-user">
                            <div class="part-img">
                                <img src="assets/img/testimonials/t-1.webp" alt="Janis Naha">
                            </div>
                            <div class="part-info">
                                <span class="name">Janis Naha</span>
                                <span class="position">Art Legends</span>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial">
                        <div class="part-body">
                            <div class="rate">
                                <ul>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p>"I am impressed and satisfied with the works Sigosoft have done for us. Sigosoft helped us by providing us Mobile and Web App for us. I recommend Sigosoft, you wont get disappointed."</p>
                        </div>
                        <div class="part-user">
                            <div class="part-img">
                                <img src="assets/img/testimonials/t-2.webp" alt="Sangeeth Moncy">
                            </div>
                            <div class="part-info">
                                <span class="name">Sangeeth Moncy</span>
                                <span class="position">calicutfish.com</span>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial">
                        <div class="part-body">
                            <div class="rate">
                                <ul>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                    <li><i class="fas fa-star"></i></li>
                                </ul>
                            </div>
                            <p>"I am very impressed with Sigosoft expertise and capability in helping us in Mobile App Development. We believe we made the right choice by choosing  Sigosoft for our product development."</p>
                        </div>
                        <div class="part-user">
                            <div class="part-img">
                                <img src="assets/img/testimonials/t-3.webp" alt="Khaleel Jibran M">
                            </div>
                            <div class="part-info">
                                <span class="name">Khaleel Jibran M</span>
                                <span class="position">CEO / Greenspark Group of Companies</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- testimonial end -->

          

        

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
    </body>


</html>