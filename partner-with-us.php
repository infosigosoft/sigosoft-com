
<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<meta property="og:locale" content="en_US"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Sigosoft|Partner With Us"/>
<meta property="og:description" content="We welcome partners to associate with us. Be our partner to work with us, invest with us and collaborate with us. For a growing company like sigosoft New York, USA."/>
<meta property="og:url" content="https://www.sigosoft.com/partner-with-us.php"/>
<meta property="og:site_name" content="Sigosoft"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@sigosoft_social">
<meta name="twitter:description" content="We welcome partners to associate with us. Be our partner to work with us, invest with us and collaborate with us. For a growing company like sigosoft New York, USA."/>
<meta name="twitter:title" content="Sigosoft|Partner With Us" />
<meta content="width=device-width,initial-scale=1,shrink-to-fit=no" name=viewport>
<title>Sigosoft|Partner With Us</title>
<meta content="We welcome partners to associate with us. Be our partner to work with us, invest with us and collaborate with us. For a growing company like sigosoft New York, USA." name=description>
<meta content="" name=keywords>
<meta name="robots" content="index, follow">
    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>



        
        <?php include('header.php');?>

        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-partner">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Partner with Us</h2>
                            <ul>
                                <li><a href=".">Home</a></li>
                                <li>Partner with Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <!-- about begin -->
        <div class="partner-page-about">
            <div class="container">
                <div class="row  justify-content-center">
                    <div class="col-xl-9 col-lg-9 col-md-12 text-center">
                        <div class="part-text">
                            <h2>We welcome partners to associate with us.</h2>
                            <p>Be our partner to work with us, invest with us and collaborate with us. For a growing company like sigosoft, we expect more interested personnel’s or groups to associate with us, that will help both of us to get more explored and achieve more heights. We can set partnerships according to some common understandings.</p>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- about end -->

        

        <!-- about-details begin -->
        <!--<div class="about-details">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-10">
                        <div class="part-text">
                            <h2 class="first-child">We provide the solutions to grow your business <span class="special">From Good to Great</span>.</h2>
                            <p>Everybody is utilizing portable applications for their business, irrespective of where they remain in their enterprising journey. Building a versatile mobile application isn't only for the enormous aggregates, the little to medium measured organizations are making up for lost time before long! Clearly, portable applications are showing improvement over versatile programs all over the place. 4 Ways your business can profit by having a portable application,</p>

                            <ul>
                                <li><i  class="fas fa-check-square"></i> They’re in all of our pockets now</li>
                                <li> <i class="fas fa-check-square"></i> Provide More Value to Your Customers</li>
                                <li><i class="fas fa-check-square"></i> Build a Stronger Brand</li>
                                <li><i class="fas fa-check-square"></i> Connect Better with Customers</li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-10">
                        <div class="part-img">
                            <img src="assets/img/bg-about4.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>-->
        <!-- about-details end -->

        <!-- choosing reason begin -->
        
        <div class="choosing-reason-about-page choosing-partner">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="single-reason">
                            <h2><i class="fas fa-briefcase"></i></h2>
                            <h3>Work With Us</h3>
                            <p>Be our partner who are willing to work with us. Work with us as a team will show the combined calibre to attain the growth.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="single-reason">
                            <h2><i class="fas fa-hand-holding-usd"></i></h2>
                            <h3>Invest With Us</h3>
                            <p>If you are a personal investor or a team, you can invest in Sigosoft private limited. As we all know our firm is a fast growing company, we welcome you with pleasure for being our partner.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-12 col-md-12">
                        <div class="single-reason">
                            <h2><i class="fas fa-share-alt"></i></h2>
                            <h3>Collaborate With Us</h3>
                            <p>We are leading firm in Mobile App development, we are ready to accepting strong partnerships with likely minded positive attitude companies. Let's built dreams together.</p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- choosing reason end -->


        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
    </body>


</html>