<?php

include "config.php";
require_once('assets/PHPMailer/src/PHPMailer.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
    {
            $secret = '6LcB5NoUAAAAAJUi9AskRBbehrNpp8oW7BvmNuVg';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success)
            {
                date_default_timezone_set('Asia/Kolkata'); 
                $name         = $_POST['name'];
                $email        = $_POST['email'];
                $position        = $_POST['position'];
                $message      = $_POST['message'];    
                $current_time = date('Y-m-d H:i:s');

                $errors= array();
                  $file_name = $_FILES['resume']['name'];
                  $file_size =$_FILES['resume']['size'];
                  $file_tmp =$_FILES['resume']['tmp_name'];
                  $file_type=$_FILES['resume']['type'];
                  $file_ext=strtolower(end(explode('.',$_FILES['resume']['name'])));
                  //echo $file_ext;
                  //exit;
                  $extensions= array("pdf","doc","docx");
                  
                  if(in_array($file_ext,$extensions)=== false){
                     $errors[]="extension not allowed, please choose a .pdf, .doc or .docx file.";
                  }
                  
                  if($file_size > 2097152){
                     $errors[]='File size must be less than 2 MB';
                  }
                  
                  if(empty($errors)==true){
                     move_uploaded_file($file_tmp,"uploads/".$file_name);
                     //echo "Success";
                     $sql = "INSERT INTO career(CareerFullName, CareerEmail, CareerPosition, CareerResume, CareerMessage, CareerDateTime) VALUES('$name','$email','$position', '$file_name', '$message', '$current_time')";
                        $result = mysqli_query($conn, $sql);
                        
                        if($result)
                        {  
                          $number = '9605107396';
                          $resp =file_get_contents("http://sms.moplet.com/api/sendhttp.php?authkey=1300ADwVfh41BpVa5c74d559&mobiles=".$number."&message=Hi%20Sigosoft,%20".$message.".%20".$name.",%20".$email.",%20".$mobile.".&sender=SGSOFT&route=4&country=0"); 

                          $site_url = "https://www.sigosoft.com/";  

                          $bodytext = '<!DOCTYPE html>
                          <html>
                          <head>
                              <meta charset="utf-8" />
                              <title>Enquiry from sigosoft.com</title>
                              <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                          </head>
                          <body style="font-family: sans-serif; color: #666; font-size: 13px; line-height: 1.5;">
                              <div style="max-width: 700px; border: 1px solid #ddd; margin: 0 auto;" >
                                  <div style = "background-color: #fafafa; padding: 10px;">
                                      <img src="' . $site_url . 'assets/img/logo-sigosoft.png" alt="Sigosoft Logo" style="max-width:170px; height:auto; margin:10px auto; display:block; "/>
                                      
                                  </div>
                                  <div style="">                              
                                      
                                      <div style="padding: 25px; background-color: #fff;"> 
                                          <h4 style="border-bottom: 1px solid #999; padding-bottom: 5px; font-size: 16px;">Message</h4>
                                          <p>Job Position: ' . $position . '</p>
                                          <p>' . $message . '</p>
                                          <p style="padding-top: 10px;">From</p>
                                          <p style="margin-bottom: -10px;">' . ucwords($name) . '</p>
                                          <p><span style="font-size: 12px;">' . $email. '</span></p>    
                                      </div>       

                                  </div>
                              </div>
                          </body>
                          </html>';

                          $email = new PHPMailer();
                          $email->SetFrom($email, $name); //Name is optional
                          $email->Subject   = 'Career Enquiry from sigosoft.com';
                          $email->Body      = $bodytext;
                          $email->AddAddress( 'mail.pressy@gmail.com');

                          $file_to_attach = 'uploads/' . $file_name;

                          $email->AddAttachment( $file_to_attach , $file_name );

                          if($email->Send()) {
                              header("location:application-received.php");
                          } else {
                              echo "Failed";
                          }

                          
                            
                        }
                        else
                        {
                            header("location:careers.php");
                        }
                  }
                  else {
                     //print_r($errors);
                    header("location:careers.php");
                  }
              }      
                    
    
}

      
 ?>