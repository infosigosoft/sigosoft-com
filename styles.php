
    <!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon-sigosoft.webp" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="assets/css/fontawesome.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="assets/fonts/flaticon.min.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <!-- aos scoll animation css -->
    <link rel="stylesheet" href="assets/css/aos.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="assets/css/style.min.css">
    <!-- responsive -->
    <link rel="stylesheet" href="assets/css/responsive.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/custom.min.css">