<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thankyou For Contacting Sigosoft</title>
    <?php include('styles.php'); ?>

        <!-- inner pages responsive css -->
        <link rel="stylesheet" href="assets/css/inner-pages-responsive.css">

    </head>
    <body>

        <!-- preloader begin -->
        <div class="preloader">

            <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
    </body>


</html>
            <div id="circle_square">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- preloader end -->

        <?php include('header.php'); ?>
        <!-- breadcrumb begin -->
        <div class="breadcrumb-murtes breadcrumb-contact">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6">
                        <div class="breadcrumb-content">
                            <h2>Contact Us</h2>
                            <ul>
                                <li><a href=".">Home</a></li>
                                <li>Contact</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb end -->

        <div class="thank-you section-bg-black text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h3><i class="far fa-check-circle"></i></h3>
                        <h2>Thank You for contacting us!</h2>
                        <p>We will contact you shortly to discuss your requirements. <br>
                        By the time, Check out our services below.</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- service begin -->
        <div class="service service-3 pt-5">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-8">
                        <div class="section-title-2 text-center mb-5">
                            <h2>Our Services</h2>
                            <!--<p>But I must explain to you how all this mistaken denouncing
                                    praising pain was born and I will give you</p>-->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6" >
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/android-app-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Android App Development
                                    <span class="bg-number">01</span>
                                </h3>
                                <p class="service-content">Android Apps is a necessity that no one can avoid especially the ones who run a business as it makes work accessible outside the office. So what are you waiting for? Call us up on your latest requirements that we can convert into an android app.</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/ios-app-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">iOS App Development
                                    <span class="bg-number">02</span>
                                </h3>
                                <p class="service-content">Want an iOS app to be developed but don't know which is the best iOS mobile app development company in Dubai,UAE? We provide the best iOS app development services to our customers and clients that help us remain as #No.1.</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/cross-platform-app-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Cross-Platform App Development
                                    <span class="bg-number">03</span>
                                </h3>
                                <p class="service-content">The fast-growing digital era, keeps inventing newer and better platforms day by day! Will my mobile app work in all of them? Yes, it will, we will make it happen at Sigosoft.</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/ecommerce-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">E-commerce Development
                                    <span class="bg-number">04</span>
                                </h3>
                                <p class="service-content">Want to build great customer traffic? We provide best e-commerce website development solutions that can help you profit faster and easier.</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/magento-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Magento Development
                                    <span class="bg-number">05</span>
                                </h3>
                                <p class="service-content">Magento platform is used for flexible and easy-to-use e-commerce website that helps improve your business.</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/cms-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">CMS Development
                                    <span class="bg-number">06</span>
                                </h3>
                                <p class="service-content">Be it a website, internet/ intranet application, Sigosoft uses CMS to create, edit or manage the content at various permission levels. Want to know how? Contact us!</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="single-servcie">
                            <div class="part-img">
                                <img src="assets/img/services/corporate-website-development.jpg" alt="">
                            </div>
                            <div class="part-text">
                                <h3 class="service-title">Corporate Website Development
                                    <span class="bg-number">07</span>
                                </h3>
                                <p class="service-content">Isn't your brand the best-seller? Then you got to need our help in managing the advertising content on your website.</p>
                                <a href="#" class="service-details-button">Discover now <i class="fas fa-long-arrow-alt-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- service end -->

        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>
    </body>


</html>